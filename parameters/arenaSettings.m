% Frequently changed parameters determining the type of the experiment ---%

    p_totLives                    = 20;

    p_cycleLength                 = 400000;%1000000;                         % At each cycle the weights are reset to their average value.

    p_STEPS                       = p_totLives * p_cycleLength;            % Total simulation steps

    p_policySwitch                = 1;                                     % 0/1 determines whether the policy is switched halfway through lifetime

    p_predictorsOnOff             = 1;

    p_visualisationDelayStepBy    = 0.001;                                 % Delay in seconds between steps

    p_plotEveryNrSteps            = 3;                                     % The arena plot is redrawn every give sim steps

    p_objectMoves                 = 0;                                     % Enable the procedure that makes object to move slowly around the arena during simulation

    p_visual                      = 1;                                     % If 0 no graphics will be displayed, otherwise the arena and simulation will be shown

    p_outputMapping               = 1;                                     % When inverted to -1, output signals have opposite effect.

    % Initial agent location in arena and heading
    x0 = rand(stream);
    y0 = rand(stream);
    angle = rand(stream) * 2 * pi;

    x0 = 0.7;
    y0 = 0.3;
    angle = 1;

% Stable parameters representing settings of the neural system and
% simulation environment -------------------------------------------------%

    p_sensorRange                 = 0.1;                                   % previously 0.1 (i.e. 10% of the arena, being the arena is 1by1)

    p_objSensorAngle              = pi/2;                                  % i.e. obj sensors are at the side

    p_objSensorDistanceFromBody   = p_sensorRange * 0.5;                   % displacement of the obj sensors from the agent body

    p_distPerStep                 = 0.0025;                                  % previously 0.01 (i.e. 1% of arena for each sim step)

    p_negMod                      = -1;                                    % modulation during wall contact

    p_posMod                      = 0.1;                            % modulation during non-wall-contanct

    neutralMod                    = p_posMod + 0.03;

    % Maximum turning angle for maximum output
    % p_maxAngle = 2 * asin((0.5 * p_distPerStep) / p_objSensorDistanceFromBody);% this value is 0.05, so 2.865 degree

                % 90 degrees in radians is 1.5708
    % p_maxAngle = 1.5708 - (acos ((0.5 * p_distPerStep) / p_objSensorDistanceFromBody)); % 1.4325 degrees


    %      this 2* is to make the maxAngle a bit bigger, to avoid the agent to turn around the sensor
    p_maxAngle = 2 * (2 * asin((0.5 * p_distPerStep) / p_objSensorDistanceFromBody)); % angle is 5.73 degree, basically pi/30


    %p_maxAngle = 0.5 * pi * p_objSensorDistanceFromBody * p_distPerStep; %questa moltiplicazione dovrebbe essere pi/50
    % p_maxAngle = pi/10;
    %p_maxAngle = pi/12; % 15deg
    %p_maxAngle = pi/15; % 12deg
    %p_maxAngle = pi/18; % 10deg
    %maxAngle = pi/20; % 9deg

    p_arenaWidth = 2.0;                                                    % Width of the arena

    p_impactRange = 0.025;                                                 % Within this distance the object is destroyed�

    p_maxObjSpeed = 0.001;

% Graphics ---------------------------------------------------------------%

    p_agentDrawRadius = .008;
    p_objDrawRadius = p_agentDrawRadius * 2;

% Object types, number and position --------------------------------------%
    answer = input(['Predictors: off (0), on (1): [default ' num2str(p_predictorsOnOff) ']: ']);
    if ~isempty(answer)
       p_predictorsOnOff = answer;
    end
    objectsInit;
    % oldApproachOfObjects;
