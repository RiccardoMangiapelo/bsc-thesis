% Allocating vector for the stimulus, or stimulus spike-train
input = zeros(p_STEPS, 1);

inputCanFire = 1;
i = 1;

while (i <= p_STEPS);   
    if (useSpikes == 1)
        % here spike are implemetend
    if (rand(stream) < probFire) && (inputCanFire == 1)
        input(i) = 1;
        inputCanFire = 0;
    else
        input(i) = notFiringPotential;
        inputCanFire = 1;
    end
    else
        % here continuous input is implemented
        input(i) = 1;
    end
    i = i + 1;
end
clear inputCanFire;
