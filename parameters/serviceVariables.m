% Logs -------------------------------------------------------------------%

    log_sensors           = zeros(p_nr_ins,p_STEPS);
    log_XYMod             = zeros(3, p_STEPS);
    log_XYModDelivered    = zeros(3, p_STEPS);

% Service variables ------------------------------------------------------%
    wallContact = 0;

    sensorA             = zeros(2,1);
    sensorB             = zeros(2,1);
    sensorC             = zeros(2,1);
    sensorD             = zeros(2,1);
    sensorE             = zeros(2,1);
    sensorF             = zeros(2,1);
    sensorG             = zeros(2,1);
    sensorH             = zeros(2,1);

    B_OffBoard(1:nr_B_objs) = 0;
    A_OffBoard(1:nr_A_objs) = 0;
    C_OffBoard(1:nr_C_objs) = 0;
    D_OffBoard(1:nr_D_objs) = 0;
    E_OffBoard(1:nr_E_objs) = 0;
    F_OffBoard(1:nr_F_objs) = 0;
    G_OffBoard(1:nr_G_objs) = 0;
    H_OffBoard(1:nr_H_objs) = 0;

    % Location of the sensors. This are recomputed at each step in agentMoves.m
    xL = x0 + ((p_objSensorDistanceFromBody) * cos(angle + p_objSensorAngle));
    yL = y0 + ((p_objSensorDistanceFromBody) * sin(angle + p_objSensorAngle));
    xR = x0 + ((p_objSensorDistanceFromBody) * cos(angle - p_objSensorAngle));
    yR = y0 + ((p_objSensorDistanceFromBody) * sin(angle - p_objSensorAngle));

    %storing position of agent
    x0Array = zeros(1,p_STEPS);
    y0Array = zeros(1,p_STEPS);

    deltaxA(1:nr_A_objs) = 0;
    deltayA(1:nr_A_objs) = 0;
    deltaxB(1:nr_B_objs) = 0;
    deltayB(1:nr_B_objs) = 0;

    distB           = zeros(nr_B_objs);
    distB_1         = zeros(nr_B_objs);

    distA           = zeros(nr_A_objs,1);
    distA_1         = zeros(nr_A_objs,1);

    distC           = zeros(nr_C_objs);
    distC_1         = zeros(nr_C_objs);

    distD           = zeros(nr_D_objs);
    distD_1         = zeros(nr_D_objs);

    distE           = zeros(nr_E_objs);
    distE_1         = zeros(nr_E_objs);

    distF           = zeros(nr_F_objs);
    distF_1         = zeros(nr_F_objs);

    distG           = zeros(nr_G_objs);
    distG_1         = zeros(nr_G_objs);

    distH           = zeros(nr_H_objs);
    distH_1         = zeros(nr_H_objs);

    distW           = 1;      % distance to the nearest wall
    distW_1         = 1;      % old distance to the nearest wall

    turnStrength    = 0;

    deltaAngle      = 0;

    modulation      = 0;

    s_step          = 2;

    simulationEnded = 0;

    % previously serviceVariablesCommon.m
    neuronInput         = zeros(p_nr_outs,p_STEPS);
    activation          = zeros(p_nr_outs,p_STEPS);
    output              = zeros(p_nr_outs,p_STEPS);
    HebbTerm            = zeros(p_nr_ins, p_nr_outs); %, p_STEPS);
    modHebbTerm         = zeros(p_nr_ins, p_STEPS);
    GLU_w               = ones(p_nr_ins, p_STEPS) * p_maxWeight/2;
    GABA_w              = ones(p_nr_ins, p_STEPS) * p_maxWeight/2;

    % Riccardo
    traceTC             = 10;%40; % This is Tc , so the steps delay of traces
    sizeOfBuffer        = 10;%30; % delay
    fifo_buffer         = zeros(1,sizeOfBuffer);
    shortTermHebb       = zeros(p_nr_ins, p_nr_outs);%,p_STEPS); % this serves as the eligibility trace
    samplingTime       = 1;%0.3; % steps. This is deltaT , so the fixed number of steps for delay
                        % 0.001 , 0.003 , 0.01 , 0.03 , 0.1 , 0.3 , 1 ...
    % update p_weightUpdateRate when
    % updating samplingTime

    %conc_of_shortTermHebb          = zeros(p_nr_ins,p_STEPS);
    A_happened       = [];
    B_happened       = [];
    C_happened       = [];
    D_happened       = [];
    E_happened       = [];
    F_happened       = [];
    G_happened       = [];
    H_happened       = [];
    all_happened     = [];

    A_happenedCurrentLife  = [];
    B_happenedCurrentLife  = [];
    C_happenedCurrentLife  = [];
    D_happenedCurrentLife  = [];
    E_happenedCurrentLife  = [];
    F_happenedCurrentLife  = [];
    G_happenedCurrentLife  = [];
    H_happenedCurrentLife  = [];

    % SOB_zeros       = zeros(1,sizeOfBuffer);
    % neg_eventSOB    = zeros(1,p_STEPS);
    % pos_eventSOB    = zeros(1,p_STEPS);

    % conc_of_steerIntensity   = zeros(1,p_STEPS);
    % conc_of_sumOfOutput      = zeros(1,p_STEPS);
    % conc_of_steerLimit       = zeros(1,p_STEPS);
    % conc_TraceB = zeros(1,p_STEPS);
    steerIntensity    = 0;
    sumOfOutput       = 0;
    spinningDecay     = 0.96;
    steeringReduction = 0.01;%1;
    steerLimit        = 20; % this value is the threshold retrieved from analysing the graphs. When the agent is spinning on itself, the steerIntensity is 2.5 assuming spinningDecay is 0.6
    t_constraint      = 0; % this variable is a sort of timer that activates when steerIntensity is above the threshold and penalises output for 30 steps (set in neuralComputation.m)
    conc_of_steerLimit(1,:) = steerLimit;
    randomNumber      = 0;
    variableDelay     = sizeOfBuffer;

    correlationLearntAt = [1];
    lastResetStep = [1];
    pS = 0;
    A_Diff_totObj = 0;
    B_Diff_totObj = 0;
    C_Diff_totObj = 0;
    D_Diff_totObj = 0;
    E_Diff_totObj = 0;
    F_Diff_totObj = 0;
    G_Diff_totObj = 0;
    H_Diff_totObj = 0;
    A_totObj = [];
    B_totObj = [];
    C_totObj = [];
    D_totObj = [];
    E_totObj = [];
    F_totObj = [];
    G_totObj = [];
    H_totObj = [];
    all_tot = [];
    finalNumOfStep = [];
    % sensConc = zeros(18,p_STEPS);

    rewardAtStep = zeros(1,p_STEPS);
    sensorBconc6 = zeros(1,p_STEPS);

    % autoencoder
    % euclideanDist                = -1;
    % sizeOfEucledianBuffer        = 40; % I choose 40 randomly
    % euclidian_buffer             = zeros(1,sizeOfEucledianBuffer);
    % sorted_buffer                = zeros(1,sizeOfEucledianBuffer);
    % sizeOfNoveltyBuffer          = 40; % I choose 40 randomly
    % novelty_buffer               = zeros(1,sizeOfNoveltyBuffer);
    % noveltyScore_newExperience   = 0;
    % leastNovel                   = sizeOfNoveltyBuffer;
