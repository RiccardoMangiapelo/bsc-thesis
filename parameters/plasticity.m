% % Hebbian term
% if p_useTanh == 1
%     if p_useSpikes == 0
%         % Riccardo: this is evaluated to true
%         HebbTerm(:,:,s_step) = p_weightUpdateRate .* sensor' * output(:,s_step)';
%     else
%         HebbTerm(:,:,s_step) = p_weightUpdateRate .* leakySensor'  * output(:,s_step)';
%     end
% else
%     HebbTerm(:,:,s_step) = p_weightUpdateRate .* tanh(sensor' * activation(:,s_step)');
% end
%
% % Modulation of Hebbian
% modHebbTerm(:,s_step) = HebbTerm(:,:,s_step) .* modulation;



% Weight update
GABA_w(:,s_step+1) = GABA_w(:,s_step) - (modHebbTerm(:,s_step)...
    .* (1 + (rand(stream, p_nr_ins, p_nr_outs) .* 2 .* p_transmissionNoise) - p_transmissionNoise));
GLU_w(:,s_step+1) = GLU_w(:,s_step) + (modHebbTerm(:,s_step)...
    .* (1 + (rand(stream, p_nr_ins, p_nr_outs) .* 2 .* p_transmissionNoise) - p_transmissionNoise));

% Weight saturation at maxWeight value and 0.0.
for i = 1:p_nr_ins
    % for j = 1:p_nr_outs
        GABA_w(i,s_step+1) = min([GABA_w(i,s_step+1) p_maxWeight]);
        GABA_w(i,s_step+1) = max([GABA_w(i,s_step+1) 0.0]);
        GLU_w(i,s_step+1) = min([GLU_w(i,s_step+1) p_maxWeight]);
        GLU_w(i,s_step+1) = max([GLU_w(i,s_step+1) 0.0]);
    % end
end
