
figHistogram1 = figure('Name','Time between events');
figure(figHistogram1);

histogram(finalNumOfStep);

% Create xlabel
xlabel('Seconds between events');

% Create ylabel
ylabel('Total events');

name = strcat('Time between events.fig');
saveas(figHistogram1,fullfile(filePath,name));
