figHistogram = figure('Name','Histogram of Saturation');
figure(figHistogram);

bins = ceil(sqrt(size(correlationLearntAt,2))); % 16/17 bins for 250 lives
if (bins < 1)
  bins = 1;
end
histogram(correlationLearntAt, bins);
% ylim([0 p_totLives+1])

% Create xlabel
xlabel('Steps to saturation');

% Create ylabel
ylabel('Lives');

name = strcat('Histogram of Saturation.fig');
saveas(figHistogram,fullfile(filePath,name));


if ( size(correlationLearntAt,2) > 2 )
    figHistDistr = figure('Name','Histogram with Distribution');
    figure(figHistDistr);

    histfit(correlationLearntAt,bins,'inversegaussian');
    % Create xlabel
    xlabel('Steps to saturation');

    % Create ylabel
    ylabel('Lives');

    name = strcat('Histogram with Distribution.fig');
    saveas(figHistDistr,fullfile(filePath,name));
end

avg_correlationLearntAt = mean(correlationLearntAt);
std_correlationLearntAt = std(correlationLearntAt);
