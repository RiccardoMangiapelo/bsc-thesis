for i=1:length(array_letters)
  nr_current_obj = 0;
  obj_around_current_obj = 0;
  switch array_letters(i)
    case 'A'
        nr_current_obj = nr_A_objs;
    case 'B'
        nr_current_obj = nr_B_objs;
    case 'C'
        nr_current_obj = nr_C_objs;
    case 'D'
        nr_current_obj = nr_D_objs;
    case 'E'
        nr_current_obj = nr_E_objs;
    case 'F'
        nr_current_obj = nr_F_objs;
    case 'G'
        nr_current_obj = nr_G_objs;
    case 'H'
        nr_current_obj = nr_H_objs;
    otherwise
        nr_current_obj = 0;
  end % of switch


  for c=1:nr_current_obj

    if (array_letters(i) == 'A')
      % place one object randomly
        random_x = rand() * ( max_widthAB - min_width_heightAB ) + min_width_heightAB; % random number in the range of max_width and min_width_height (plus the angel_devil_area)
        random_y = rand() * ( max_heightAB - min_width_heightAB ) + min_width_heightAB;
        k = 1;
        while ( k<=length(existing_x) )
          if ( ismember(existing_x(k),xA) ) % if existing_x(k) is in xA, then the object is of type A
            far_away_value = (distance_between_objects*2) - tenSecs;%distance_within_vip;

          elseif ( ismember(existing_x(k),xB) ) % if existing_x(k) is in xB, then the object is of type B
            far_away_value = (BA_dist * 2) + distance_between_objects;

          else
            far_away_value = distance_between_objects;

          end % of ismember if

          if (abs( random_x - existing_x(k) ) <= far_away_value & ...
              abs( random_y - existing_y(k) ) <= far_away_value)

              random_x = rand() * ( max_widthAB - min_width_heightAB ) + min_width_heightAB;
              random_y = rand() * ( max_heightAB - min_width_heightAB ) + min_width_heightAB;
              k = 1;
          else
              k = k+1;
          end % of abs if

        end % of k loop

        % store newly calculated x and y to existing_x and existing_y
        existing_x(end+1)= random_x;
        existing_y(end+1)= random_y;
        % store newly calculated x and y to object array
        xA(c) = random_x;
        yA(c) = random_y;

    elseif (array_letters(i) == 'B')
      % place one object randomly
        random_x = rand() * ( max_widthAB - min_width_heightAB ) + min_width_heightAB; % random number in the range of max_width and min_width_height (plus the angel_devil_area)
        random_y = rand() * ( max_heightAB - min_width_heightAB ) + min_width_heightAB;
        k = 1;
        while ( k<=length(existing_x) )
          if ( ismember(existing_x(k),xB) ) % if existing_x(k) is in xB, then the object is of type B
            far_away_value = (distance_between_objects*2) - tenSecs;%distance_within_vip;

          elseif ( ismember(existing_x(k),xA) ) % if existing_x(k) is in xA, then the object is of type A
            far_away_value = (BA_dist * 2) + distance_between_objects;

          else
            far_away_value = distance_between_objects;

          end % of ismember if

          if (abs( random_x - existing_x(k) ) <= far_away_value & ...
              abs( random_y - existing_y(k) ) <= far_away_value)

              random_x = rand() * ( max_widthAB - min_width_heightAB ) + min_width_heightAB;
              random_y = rand() * ( max_heightAB - min_width_heightAB ) + min_width_heightAB;
              k = 1;
          else
              k = k+1;
          end % of abs if

        end % of k loop

        % store newly calculated x and y to existing_x and existing_y
        existing_x(end+1)= random_x;
        existing_y(end+1)= random_y;
        % store newly calculated x and y to object array
        xB(c) = random_x;
        yB(c) = random_y;

    elseif (array_letters(i) == 'C')

      flag = false;
      [receiver_xA,idx] = datasample(xA,1); %picks a random value in the array and stores it in receiver_xA and its index in idx
      % if the number of indexes in array is less than maxNumb_ofPredictors (i.e. we used this index more less maxNumb_ofPredictors times)
      while (~flag) % until it is true
        if (numel(find(idx == how_many_times_xA_used)) < maxNumb_ofPredictors) % how many times idx is present in the array
          how_many_times_xA_used(end+1) = idx;
          flag = true;
        else
          [receiver_xA,idx] = datasample(xA,1);
        end
      end
      receiver_yA = yA(idx); % get the equivalent y value of the randomly picked x above

        random_x = rand() * ((receiver_xA + angel_devil_area) - (receiver_xA - angel_devil_area)) + (receiver_xA - angel_devil_area); % random number in the range of beginning and end of devil area
        random_y = rand() * ((receiver_yA + angel_devil_area) - (receiver_yA - angel_devil_area)) + (receiver_yA - angel_devil_area);
        k = 1;
        while ( k<=length(existing_x) )

          if (abs( random_x - existing_x(k) ) <= distance_within_vip &&...
             abs( random_y - existing_y(k) ) <= distance_within_vip) || ...
             ~(random_x > min_width_height && random_x < max_width) || ...
             ~(random_y > min_width_height && random_y < max_height)
                  random_x = rand() * ((receiver_xA + angel_devil_area) - (receiver_xA - angel_devil_area)) + (receiver_xA - angel_devil_area);
                  random_y = rand() * ((receiver_yA + angel_devil_area) - (receiver_yA - angel_devil_area)) + (receiver_yA - angel_devil_area);
                  k = 1;
          else
                  k = k+1;
          end

        end
        existing_x(end+1)= random_x;
        existing_y(end+1)= random_y;

        xC(c) = random_x;
        yC(c) = random_y;

    elseif (array_letters(i) == 'D')

      flag = false;
      [receiver_xB,idx] = datasample(xB,1); %picks a random value in the array and stores it in receiver_xA and its index in idx
      % if the number of indexes in array is less than maxNumb_ofPredictors (i.e. we used this index more less maxNumb_ofPredictors times)
      while (~flag) % until it is true
        if (numel(find(idx == how_many_times_xB_used)) < maxNumb_ofPredictors) % how many times idx is present in the array
          how_many_times_xB_used(end+1) = idx;
          flag = true;
        else
          [receiver_xB,idx] = datasample(xB,1);
        end
      end
      receiver_yB = yB(idx); % get the equivalent y value of the randomly picked x above

        random_x = rand() * ((receiver_xB + angel_devil_area) - (receiver_xB - angel_devil_area)) + (receiver_xB - angel_devil_area); % random number in the range of beginning and end of devil area
        random_y = rand() * ((receiver_yB + angel_devil_area) - (receiver_yB - angel_devil_area)) + (receiver_yB - angel_devil_area);
        k = 1;
        while ( k<=length(existing_x) )

          if (abs( random_x - existing_x(k) ) <= distance_within_vip && ...
             abs( random_y - existing_y(k) ) <= distance_within_vip) || ...
             ~(random_x > min_width_height && random_x < max_width) || ...
             ~(random_y > min_width_height && random_y < max_height)
                  random_x = rand() * ((receiver_xB + angel_devil_area) - (receiver_xB - angel_devil_area)) + (receiver_xB - angel_devil_area);
                  random_y = rand() * ((receiver_yB + angel_devil_area) - (receiver_yB - angel_devil_area)) + (receiver_yB - angel_devil_area);
                  k = 1;
          else
                  k = k+1;
          end

        end
        existing_x(end+1)= random_x;
        existing_y(end+1)= random_y;

        xD(c) = random_x;
        yD(c) = random_y;

    else
      % place neutral objects randomly
        random_x = rand() * ( max_width - min_width_height ) + min_width_height; % random number in the range of max_width and min_width_height (plus the angel_devil_area)
        random_y = rand() * ( max_height - min_width_height ) + min_width_height;
        k = 1;
        while ( k<=length(existing_x) )
          if ( ismember(existing_x(k),xA) | ismember(existing_x(k),xB) )
            far_away_value = angel_devil_area + distance_within_vip;
          else
            far_away_value = distance_between_objects;
          end % of ismember if

          if (abs( random_x - existing_x(k) ) <= far_away_value & ...
              abs( random_y - existing_y(k) ) <= far_away_value)

              random_x = rand() * ( max_width - min_width_height ) + min_width_height;
              random_y = rand() * ( max_height - min_width_height ) + min_width_height;
              k = 1;
          else
              k = k+1;
          end % of abs if

        end % of k loop

        % store newly calculated x and y to existing_x and existing_y
        existing_x(end+1)= random_x;
        existing_y(end+1)= random_y;
        % store newly calculated x and y to object array
        eval(['x' array_letters(i) '(c)' '=random_x;']);
        eval(['y' array_letters(i) '(c)' '=random_y;']);

    end


  end % of c loop

end % of i loop
