% Drawing fixed parameters -----------------------------------------------%

    % GLU-GABA sensor clusters
        heightS = 0.8;

        LWgl = -.5 - 0.13;
        LWga = -.5 - 0.1;

        RWgl = -.5 + 0.1;
        RWga = -.5 + 0.13;

        LAgl = -.5 - 0.25;
        LAga = -.5 - 0.22;

        RAgl = -.5 + 0.22;
        RAga = -.5 + 0.25;

        LBgl = -.5 - 0.37;
        LBga = -.5 - 0.34;

        RBgl = -.5 + 0.34;
        RBga = -.5 + 0.37;

% Drawing static objects (no handle needed) ------------------------------%

scrsz = get(0,'ScreenSize');
%f = figure('Position',[scrsz(3) * 0.2 1000 1300 600], 'Resize', 'off');
%f = figure('Position',[0 800 1300 600], 'Resize', 'off', 'DockControls','off');
%f = figure('DockControls','off');

f = figure('Position',[0 0 1300 600], 'Resize', 'off', 'DockControls','off', 'Name','Distal reward problem applied to Hebbian Network');

% Drawing area -----------------------------------------------------------%
axis([-1.2 2.1 -0.8 1.2]);
rectangle('Position',[-1.2, -0.4, 3.3, 1.6],'facecolor','w');

outerpos = get(gca, 'outerposition');
insetpos = get(gca, 'tightinset');
set(gca, 'position', [insetpos(1) insetpos(2) ...
    outerpos(3)-insetpos(3) outerpos(4)-insetpos(4)]);
axis fill;
axis off;
axis equal;
axis manual;

text(-1 ,1.15,'The agent reinforces actions that are positively modulated, and extinguishes the others', 'FontSize',12);
text(0 ,-0.1,'Pos mod on approach: ', 'FontSize',11);
text(0 ,-0.16,'Neg mod on approach: ', 'FontSize',11);
text(0 ,-0.22,'Neu mod on approach: ', 'FontSize',11);
text(-1.1,0.45,'GABA connection');
text(-1.1,0.30,'GLU connection');
% text(0,1.05,'Simulation step: ');
text(0.85,-0.09,'Speed of simulation');
text(-0.13,0.53,'+Mod');
text(-0.12,0.06,'-Mod');
text(-0.16,0.3,'0');
text(-0.75 ,0.9,'LEFT          |          RIGHT','FontSize',12);

% Control bar ------------------------------------------------------------%

graphRefresh = uicontrol(f, 'Style', 'slider',...
 'Min',0,'Max',9,'Value',3,...
        'Position', [780 210 120 10]);

text(0.65,-0.3,'Policy');

% Create the button group.                                      [left bottom width height]
h = uibuttongroup('visible','off','Units', 'inches', 'Position',[10.6 1.88 1 0.75], 'BorderType', 'none');
% Create three radio buttons in the button group.
%u0 = uicontrol('Style','Radio','String','Auto',...
%    'Value', 1, 'Selected', 'on', 'SelectionHighlight', 'on',...
%    'pos',[750 170 120 30],'parent',h,'HandleVisibility','off');
u1 = uicontrol('Style','Radio','String','Policy 1',...
    'pos',[0 25 120 30],'parent',h,'HandleVisibility','off', ...
    'BackgroundColor','w');
u2 = uicontrol('Style','Radio','String','Policy 2',...
    'pos',[0 0 120 30],'parent',h,'HandleVisibility','off', ...
    'BackgroundColor','w');
% Initialize some button group properties.
%set(h,'SelectionChangeFcn',@selcbk);
set(h,'SelectedObject',[]);  % No selection
set(h,'Visible','on');
if modulationPolicy == 1
    set(u1, 'Value',1);
    set(u2, 'Value',0);
else
    set(u2, 'Value',1);
    set(u1, 'Value',0);
end


resetWeightsButton = uicontrol(f, 'Style', 'pushbutton',...
        'String', 'Reset Weights',...
        'Position', [1000 170 100 30],...
        'Callback', 'resetWeights');

invertOutButton = uicontrol(f, 'Style', 'pushbutton',...
        'String', 'Invert Out',...
        'Position', [1000 200 100 30],...
        'Callback', 'invertOut');
endSimulationButton = uicontrol(f, 'Style', 'pushbutton',...
        'String', 'End Simulation',...
        'Position', [1000 140 100 30],...
        'Callback', 'endSimulation');

%                                                                [left bottom width height]
h2 = uibuttongroup('visible','off','Units', 'inches', 'Position',[12.1 1.88 1.4 0.75], 'BorderType', 'none');
% Create three radio buttons in the button group.
ub0 = uicontrol('Style','Radio','String','Static objs',...
    'Value', 1, 'Selected', 'on', 'SelectionHighlight', 'on',...
    'pos',[0 25 120 30],'parent',h2,'HandleVisibility','off',...
    'BackgroundColor','w');
ub1 = uicontrol('Style','Radio','String','Moving objs',...
    'pos',[0 0 120 30],'parent',h2,'HandleVisibility','off',...
    'BackgroundColor','w');
% Initialize some button group properties.
%set(h,'SelectionChangeFcn',@selcbk);
set(h2,'SelectedObject',[]);  % No selection
set(h2,'Visible','on');
set(ub0, 'Value',1);


% Arena
rectangle('Position',[-.01,-.01,p_arenaWidth+.01,1+.01],'facecolor','w');

% GABA and GLU example lines
line([-1.1 -1],[0.5 0.5],[1 1], 'Color','blue','LineWidth',5);
line([-1.1 -1],[0.35 0.35],[1 1], 'Color','red','LineWidth',5);

% Small red and blue rectangles
rectangle('Position',[LWgl, heightS, .02, .025],'facecolor','r');
rectangle('Position',[LWga, heightS, .02, .025],'facecolor','b');
text(LWgl,heightS + .27,'W', 'FontSize',11);

rectangle('Position',[RWgl, heightS, .02, .025],'facecolor','r');
rectangle('Position',[RWga, heightS, .02, .025],'facecolor','b');
text(RWgl,heightS + .27,'W', 'FontSize',11);

rectangle('Position',[LBgl, heightS, .02, .025],'facecolor','r');
rectangle('Position',[LBga, heightS, .02, .025],'facecolor','b');
text(LBgl,heightS + .27,'B', 'FontSize',11);

rectangle('Position',[RBgl, heightS, .02, .025],'facecolor','r');
rectangle('Position',[RBga, heightS, .02, .025],'facecolor','b');
text(RBgl,heightS + .27,'B', 'FontSize',11);

rectangle('Position',[LAgl, heightS, .02, .025],'facecolor','r');
rectangle('Position',[LAga, heightS, .02, .025],'facecolor','b');
text(LAgl,heightS + .27,'A', 'FontSize',11);

rectangle('Position',[RAgl, heightS, .02, .025],'facecolor','r');
rectangle('Position',[RAga, heightS, .02, .025],'facecolor','b');
text(RAgl,heightS + .27,'A', 'FontSize',11);

%rectangle('Position',[LBgl, heightS+0.04, RBga+0.03-LBgl, abs(.2 * 1)+.001],'facecolor','w','linestyle','--');

% modulation
rectangle('Position',[-.1, .1, .06, abs(.2 * 2)+.001],'facecolor','w');
% modulation 0 line
line([-0.12 -0.05],[0.3 0.3], 'Color',[0 0 0]);

% output
rectangle('Position',[-.5-0.2, .2, 0.4,.06],'facecolor','w');
text(-0.7,0.17,'Activation output neuron', 'FontSize',10);
text(-0.65,0.24,'INHIB.    EXCIT.', 'FontSize',11);

text(-1.1,-0.1,'\Delta w_i = input_i(t-1) \cdot output(t) \cdot modulation', 'FontSize',12);
text(-1.1,0.1,['Weights:        LW      RW       LA      RA      LB      RB']);


% Dynamic object drawing with hanled for later redrawing -----------------%
positiveObject = rectangle('Position',[0.4, -0.12 , 2* p_objDrawRadius, 2 * p_objDrawRadius], 'Linewidth', 2 ,'facecolor','g');
negativeObject = rectangle('Position',[0.4, -0.18 , 2* p_objDrawRadius, 2 * p_objDrawRadius], 'Linewidth', 2 , 'facecolor','m');
neutralObject = rectangle('Position',[0.4, -0.24 , 2* p_objDrawRadius, 2 * p_objDrawRadius], 'Linewidth', 2 , 'facecolor',[1 1 0.96], 'Curvature',[1,1]);
neutralObject = rectangle('Position',[0.4, -0.30 , 2* p_objDrawRadius, 2 * p_objDrawRadius], 'Linewidth', 2 , 'facecolor',[1 1 0.96], 'Curvature',[0,0]);
neutralObject = rectangle('Position',[0.4, -0.36 , 2* p_objDrawRadius, 2 * p_objDrawRadius], 'Linewidth', 2 , 'facecolor',[1 1 0.96], 'Curvature',[1,1], 'linestyle',':');
neutralObject = rectangle('Position',[0.47, -0.24 , 2* p_objDrawRadius, 2 * p_objDrawRadius], 'Linewidth', 2 , 'facecolor',[1 1 0.96], 'Curvature',[0,0], 'linestyle',':');
neutralObject = rectangle('Position',[0.47, -0.30 , 2* p_objDrawRadius, 2 * p_objDrawRadius], 'Linewidth', 2 , 'facecolor',[1 1 0.96], 'Curvature',[1,1], 'linestyle','-.');
neutralObject = rectangle('Position',[0.47, -0.36 , 2* p_objDrawRadius, 2 * p_objDrawRadius], 'Linewidth', 2 , 'facecolor',[1 1 0.96], 'Curvature',[0,0], 'linestyle','-.');

GLU_1 = line([-0.52 LWgl],[0.3 heightS],[1 1], 'Color','r','LineWidth',GLU_w(1,s_step)+.001);
GABA_1 = line([-0.51 LWga],[0.3 heightS],[1 1], 'Color','b', 'LineWidth',GABA_w(1,s_step)+.001);
GLU_2 = line([-0.49 RWgl],[0.3 heightS],[1 1], 'Color','r', 'LineWidth',GLU_w(2,s_step)+.001);
GABA_2 = line([-0.48 RWga],[0.3 heightS],[1 1], 'Color','b', 'LineWidth',GABA_w(2,s_step)+.001);
GLU_3 = line([-0.56 LAgl],[0.3 heightS],[1 1], 'Color','r', 'LineWidth',GLU_w(3,s_step)+.001);
GABA_3 = line([-0.54 LAga],[0.3 heightS],[1 1], 'Color','b', 'LineWidth',GABA_w(3,s_step)+.001);
GLU_4 = line([-0.46 RAgl],[0.3 heightS],[1 1], 'Color','r', 'LineWidth',GLU_w(4,s_step)+.001);
GABA_4 = line([-0.44 RAga],[0.3 heightS],[1 1], 'Color','b', 'LineWidth',GABA_w(4,s_step)+.001);
GLU_5 = line([-0.60 LBgl],[0.3 heightS],[1 1], 'Color','r', 'LineWidth',GLU_w(5,s_step)+.001);
GABA_5 = line([-0.58 LBga],[0.3 heightS],[1 1], 'Color','b', 'LineWidth',GABA_w(5,s_step)+.001);
GLU_6 = line([-0.42 RBgl],[0.3 heightS],[1 1], 'Color','r', 'LineWidth',GLU_w(6,s_step)+.001);
GABA_6 = line([-0.4 RBga],[0.3 heightS],[1 1], 'Color','b', 'LineWidth',GABA_w(6,s_step)+.001);


% OBJECTS ----------------------------------------------------------------%

placeObjectsOnWindow;

%Agent's body %-----------------------------------------------------------%
    a_body = rectangle('Position',[x0 - p_agentDrawRadius,y0 - p_agentDrawRadius,2 * p_agentDrawRadius,2 * p_agentDrawRadius],...
        'Curvature',[1,1], 'facecolor','b');

    leftSensor = rectangle('Position',[xL-.01,yL-.01,.01,.01],'Curvature',[1,1], 'facecolor','b');
    rightSensor = rectangle('Position',[xR-.01,yR-.01,.01,.01],'Curvature',[1,1], 'facecolor','b');

% Sensor Bars ------------------------------------------------------------%
    sensorLWbar = rectangle('Position',[LWgl, heightS+0.04, .06, .001],'facecolor',[0.3 0.3 0.3],'linestyle','none');
    sensorRWbar = rectangle('Position',[RWgl, heightS+0.04, .06, .001],'facecolor',[0.3 0.3 0.3],'linestyle','none');
    sensorLAbar = rectangle('Position',[LAgl, heightS+0.04, .06, .001],'facecolor',[0.3 0.3 0.3],'linestyle','none');
    sensorRAbar = rectangle('Position',[RAgl, heightS+0.04, .06, .001],'facecolor',[0.3 0.3 0.3],'linestyle','none');
    sensorLBbar = rectangle('Position',[LBgl, heightS+0.04, .06, .001],'facecolor',[0.3 0.3 0.3]);
    sensorRBbar = rectangle('Position',[RBgl, heightS+0.04, .06, .001],'facecolor',[0.3 0.3 0.3]);

% Modulation bar ---------------------------------------------------------%
    positiveModulationBar = rectangle('Position',[-.1, .3, .06, .001],'facecolor',[.4 1 .4]);
    negativeModulationBar = rectangle('Position',[-.1, .3 - .001, .06, .001],'facecolor',[.4 .4 .4]);

% Output bar -------------------------------------------------------------%
    excitatoryOutputBar = rectangle('Position',[-.5, .2, .001,.06],'facecolor','r','linestyle','none');
    inhibitoryOutputBar = rectangle('Position',[-.5 - 0.001, .2, .001,.06],'facecolor','b','linestyle','none');

% Text -------------------------------------------------------------------%
    % simulationStepInText = text(0.30,1.05,num2str(s_step));

    speedOfSimulationText = text(1,-0.19, num2str(p_plotEveryNrSteps));

    glu_w_text_handle = text(-1,0.06,'', 'FontSize',11);
    gaba_w_text_handle = text(-1,0.0,'', 'FontSize',11);
