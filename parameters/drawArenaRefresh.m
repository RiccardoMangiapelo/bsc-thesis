% Redraw changing parts of the arena


% Object appearance ------------------------------------------------------%
    for i = 1:nr_A_objs
        if (A_OffBoard(i) == 1)
            set(objA(i), 'facecolor','w', 'linestyle','none');
        else
            if modulationPolicy == 1
                set(objA(i),'facecolor',[1 0.75 1], 'linestyle','-');
            else
                set(objA(i),'facecolor',[0.75 1 0.75], 'linestyle','-');
            end
        end
    end

    for i = 1:nr_B_objs
        if (B_OffBoard(i) == 1)
            set(objB(i), 'facecolor','w', 'linestyle','none');
        else
            if modulationPolicy == 1
                set(objB(i),'facecolor',[0.75 1 0.75], 'linestyle', '-');
            else
                set(objB(i),'facecolor',[1 0.75 1], 'linestyle', '-');
            end
        end
    end

    for i = 1:nr_C_objs
        if (C_OffBoard(i) == 1)
            set(objC(i), 'facecolor','w', 'linestyle','none');
        else
            set(objC(i),'facecolor',[1 1 0.96], 'linestyle','-');
        end
    end

    for i = 1:nr_D_objs
        if (D_OffBoard(i) == 1)
            set(objD(i), 'facecolor','w', 'linestyle','none');
        else
            set(objD(i),'facecolor',[1 1 0.96], 'linestyle','-');
        end
    end

    for i = 1:nr_E_objs
        if (E_OffBoard(i) == 1)
            set(objE(i), 'facecolor','w', 'linestyle','none');
        else
            set(objE(i),'facecolor',[1 1 0.96], 'linestyle',':');
        end
    end

    for i = 1:nr_F_objs
        if (F_OffBoard(i) == 1)
            set(objF(i), 'facecolor','w', 'linestyle','none');
        else
            set(objF(i),'facecolor',[1 1 0.96], 'linestyle',':');
        end
    end

    for i = 1:nr_G_objs
        if (G_OffBoard(i) == 1)
            set(objG(i), 'facecolor','w', 'linestyle','none');
        else
            set(objG(i),'facecolor',[1 1 0.96], 'linestyle','-.');
        end
    end

    for i = 1:nr_H_objs
        if (H_OffBoard(i) == 1)
            set(objH(i), 'facecolor','w', 'linestyle','none');
        else
            set(objH(i),'facecolor',[1 1 0.96], 'linestyle','-.');
        end
    end

if (modulationPolicy == 1)
%    set(positiveObject, 'facecolor',[1 1 0]);
%    set(negativeObject, 'facecolor',[0.6 0.6 1]);
     set(positiveObject, 'Curvature',[0,0]);
     set(negativeObject, 'Curvature',[1,1]);
else
     set(positiveObject, 'Curvature',[1,1]);
     set(negativeObject, 'Curvature',[0,0]);
%    set(positiveObject, 'facecolor',[0.6 0.6 1]);
%    set(negativeObject, 'facecolor',[1 1 0]);
end

% Agent position ---------------------------------------------------------%
    set(a_body,'Position',[x0 - p_agentDrawRadius,y0 - p_agentDrawRadius,2 * p_agentDrawRadius,2 * p_agentDrawRadius]);

    set(leftSensor, 'Position',[xL-.01,yL-.01,.01,.01]);
    set(rightSensor, 'Position',[xR-.01,yR-.01,.01,.01]);

% Sensor bars ------------------------------------------------------------%
    set(sensorLWbar, 'Position',[LWgl, heightS+0.04, .06, abs(.2 * sensor(1))+.001]);
    set(sensorRWbar, 'Position',[RWgl, heightS+0.04, .06, abs(.2 * sensor(2))+.001]);
    set(sensorLAbar, 'Position',[LAgl, heightS+0.04, .06, abs(.2 * sensor(3))+.001]);
    set(sensorRAbar, 'Position',[RAgl, heightS+0.04, .06, abs(.2 * sensor(4))+.001]);
    set(sensorLBbar, 'Position',[LBgl, heightS+0.04, .06, abs(.2 * sensor(5))+.001]);
    set(sensorRBbar, 'Position',[RBgl, heightS+0.04, .06, abs(.2 * sensor(6))+.001]);

% Length of the modulation bar -------------------------------------------%
    if (fifo_buffer(sizeOfBuffer) >= 0)
        posModBarLength = abs(.2 * fifo_buffer(sizeOfBuffer))+.001;
        negModBarLength = 0.001;
    else
        posModBarLength = 0.001;
        negModBarLength = abs(.2 * fifo_buffer(sizeOfBuffer))+.001;
    end

    set(positiveModulationBar, 'Position',[-.1, .3, .06, posModBarLength]);
    set(negativeModulationBar, 'Position',[-.1, .3 - negModBarLength, .06, negModBarLength]);

% Output bar -------------------------------------------------------------%
    excitatoryOutValue = max([ output(1,s_step)*0.2 0.001]);
    inhibitoryOutValue = -min([output(1,s_step)*0.2 -0.001]);

    set(excitatoryOutputBar, 'Position',[-.5, .2, excitatoryOutValue,.03]);
    set(inhibitoryOutputBar, 'Position',[-.5 - inhibitoryOutValue, .2, inhibitoryOutValue,.03]);


% Text -------------------------------------------------------------------%
    % set(simulationStepInText, 'String', num2str(s_step));

    set(speedOfSimulationText, 'String', num2str(speedValue));

    glu_w_text = ['  GLU : ' num2str(GLU_w(1,s_step),'%2.2f') '  ' num2str(GLU_w(2,s_step),'%4.2f')...
        '  ' num2str(GLU_w(3,s_step),'%4.2f') '  ' num2str(GLU_w(4,s_step),'%4.2f') '  ' num2str(GLU_w(5,s_step),'%4.2f')...
        '  ' num2str(GLU_w(6,s_step),'%4.2f') ];

    set(glu_w_text_handle, 'String', glu_w_text);

    gaba_w_text = ['GABA: ' num2str(GABA_w(1,s_step),'%4.2f') '  ' num2str(GABA_w(2,s_step),'%4.2f')...
        '  ' num2str(GABA_w(3,s_step),'%4.2f') '  ' num2str(GABA_w(4,s_step),'%4.2f') '  ' num2str(GABA_w(5,s_step),'%4.2f')...
        '  ' num2str(GABA_w(6,s_step),'%4.2f') ];

    set(gaba_w_text_handle, 'String', gaba_w_text);

% Connection lines -------------------------------------------------------%
    set(GLU_1, 'LineWidth',GLU_w(1,s_step) +.001);
    set(GABA_1,'LineWidth',GABA_w(1,s_step)+.001);
    set(GLU_2, 'LineWidth',GLU_w(2,s_step) +.001);
    set(GABA_2,'LineWidth',GABA_w(2,s_step)+.001);
    set(GLU_3, 'LineWidth',GLU_w(3,s_step) +.001);
    set(GABA_3,'LineWidth',GABA_w(3,s_step)+.001);
    set(GLU_4, 'LineWidth',GLU_w(4,s_step) +.001);
    set(GABA_4,'LineWidth',GABA_w(4,s_step)+.001);
    set(GLU_5, 'LineWidth',GLU_w(5,s_step) +.001);
    set(GABA_5,'LineWidth',GABA_w(5,s_step)+.001);
    set(GLU_6, 'LineWidth',GLU_w(6,s_step) +.001);
    set(GABA_6,'LineWidth',GABA_w(6,s_step)+.001);

drawnow;

pause(p_visualisationDelayStepBy);
