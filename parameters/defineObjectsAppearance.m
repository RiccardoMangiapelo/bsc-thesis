for i = 1:nr_A_objs
    if (A_OffBoard(i) == 1)
        set(objA(i), 'facecolor','w', 'linestyle','none');
    else
        if modulationPolicy == 1
            set(objA(i),'facecolor',[1 0.75 1], 'linestyle','-');
        else
            set(objA(i),'facecolor',[0.75 1 0.75], 'linestyle','-');
        end
    end
end

for i = 1:nr_B_objs
    if (B_OffBoard(i) == 1)
        set(objB(i), 'facecolor','w', 'linestyle','none');
    else
        if modulationPolicy == 1
            set(objB(i),'facecolor',[0.75 1 0.75], 'linestyle', '-');
        else
            set(objB(i),'facecolor',[1 0.75 1], 'linestyle', '-');
        end
    end
end

for i = 1:nr_C_objs
    if (C_OffBoard(i) == 1)
        set(objC(i), 'facecolor','w', 'linestyle','none');
    else
        set(objC(i),'facecolor',[1 1 0.96], 'linestyle','-');
    end
end

for i = 1:nr_D_objs
    if (D_OffBoard(i) == 1)
        set(objD(i), 'facecolor','w', 'linestyle','none');
    else
        set(objD(i),'facecolor',[1 1 0.96], 'linestyle','-');
    end
end

for i = 1:nr_E_objs
    if (E_OffBoard(i) == 1)
        set(objE(i), 'facecolor','w', 'linestyle','none');
    else
        set(objE(i),'facecolor',[1 1 0.96], 'linestyle',':');
    end
end

for i = 1:nr_F_objs
    if (F_OffBoard(i) == 1)
        set(objF(i), 'facecolor','w', 'linestyle','none');
    else
        set(objF(i),'facecolor',[1 1 0.96], 'linestyle',':');
    end
end

for i = 1:nr_G_objs
    if (G_OffBoard(i) == 1)
        set(objG(i), 'facecolor','w', 'linestyle','none');
    else
        set(objG(i),'facecolor',[1 1 0.96], 'linestyle','-.');
    end
end

for i = 1:nr_H_objs
    if (H_OffBoard(i) == 1)
        set(objH(i), 'facecolor','w', 'linestyle','none');
    else
        set(objH(i),'facecolor',[1 1 0.96], 'linestyle','-.');
    end
end
