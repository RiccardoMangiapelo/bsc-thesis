startFrom = correlationLearntAt(end);% Original value was 3000 because that was when the learned behaviour was stable (i.e. correlation was learnt)
stopAt = size(log_XYMod,2);
gridResolutionX = 200;
gridResolutionY = 100;
clear probDensity;
probDensity = zeros(gridResolutionX*2, gridResolutionY);

for i =  startFrom:stopAt
%                                                     0.5 needed to round up
    xCoord = round(log_XYMod(1,i) * gridResolutionX + 0.5);
    yCoord = round(log_XYMod(2,i) * gridResolutionY + 0.5);
    probDensity(xCoord, yCoord) = probDensity(xCoord, yCoord) + 1;

end

figFirstDensity = figure('Name','Temperature plot');
figure(figFirstDensity);
surf(log(probDensity+2), 'LineStyle','none');
view([90 90]);
axis off;
set(gca,'xdir','reverse')
% axis equal;
%colormap('gray')
name = strcat('Temperature Color.fig');
saveas(figFirstDensity,fullfile(filePath,name));

figSecondDensity = figure('Name','Temperature plot');
figure(figSecondDensity);
surf(log(probDensity+2), 'LineStyle','none');
view([90 90]);
axis off;
set(gca,'xdir','reverse')
% axis equal;
colormap('gray')
name = strcat('Temperature BW.fig');
saveas(figSecondDensity,fullfile(filePath,name));

% OR
% figThirdDensity = figure('Name','Temperature plot');
% figure(figThirdDensity);
% imagesc(log(probDensity+1'));
% axis equal;
% colormap('gray')


clear startFrom;
clear stopAt;
clear xCoord;
clear yCoord;
clear gridResolution;
