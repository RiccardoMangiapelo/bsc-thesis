figSteerIntensity = figure('Name','Steer intensity');

figure(figSteerIntensity);

ax1 = subplot(2,1,1);
plot(conc_of_sumOfOutput(1:s_step));
ylabel('Without decay');

ax2 = subplot(2,1,2);
plot(conc_of_steerLimit(1:s_step),'r')
hold all
plot(-conc_of_steerLimit(1:s_step),'r')
plot(conc_of_steerIntensity(1:s_step),'b');
axis([0,s_step,-25,25]);
ylabel('steerIntensity (with decay)');

name = strcat('Steer intensity.fig');
saveas(figSteerIntensity,fullfile(filePath,name));
