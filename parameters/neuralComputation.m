if (p_useSpikes == 1)
    leakySensor = leakySensor + (-leakySensor + sensor)/4;
    sensor = leakySensor;
end

% ALL INPUTS -------------------------------------------------------------%
neuronInput(:,s_step) = sensor * GLU_w(:,s_step)...
    - sensor * GABA_w(:,s_step);

% ACTIVATIONS (continuous mode) ------------------------------------------%
activation(:,s_step) = activation(:,s_step - 1) ...
    + (neuronInput(:,s_step) - activation(:,s_step - 1)) ./ p_tau;

% Sigmoid or Tanh --------------------------------------------------------%

% Riccardo
% Stuck-in-circle

if (p_useTanh == 0)
    output(:,s_step) = 1 / (1 + exp(-activation(:,s_step)));
else
  % t_constraint set to 30 steps because agent does not perceive object anymore after 20 steps (given by step length [p_distPerStep] / distance of sensor from body [p_objSensorDistanceFromBody]), so 30 is just a safe value greater than 20
  if ((steerIntensity < -steerLimit | steerIntensity > steerLimit) && t_constraint==0) || (t_constraint<=30 && t_constraint>=1)
    % output times a penalty
    output(:,s_step) = steeringReduction * tanh(activation(:,s_step));
    t_constraint=t_constraint+1;
    t_constraint=t_constraint*(t_constraint<31); % if t_constraint is > 30 then t_constraint = 0;
  else
     output(:,s_step) = tanh(activation(:,s_step));
  end
  steeringNoise = ( 0.1 * (1 - 2*rand(1,1)));
  steerIntensity = steerIntensity * spinningDecay + output(:,s_step) + steeringNoise ;
  sumOfOutput = sumOfOutput + output(:,s_step);
end


% SPIKES -----------------------------------------------------------------%
if (p_useSpikes == 1)
    % Condition 1 is that the neuron has not fired at the previous
    % time-step
    canFireCondition1 = zeros(1,p_nr_outs);
    canFireCondition1(find(output(:,s_step - 1) < 0.5)) = 1;
    % Condition 2 is that the activation value (or membrane potential) is
    % greater than 0
    canFireCondition2 = zeros(1,p_nr_outs);
    canFireCondition2(find(activation(:,s_step) > 0)) = 1;

    % When the two conditions are verified, a spike is fired with
    % probability p_probSpike
    canFire = canFireCondition1 .* canFireCondition2 .* (rand(stream, p_nr_outs, 1) < p_probSpike);
    %fire = canFire .* (output(:,s_step) > 0);

    output(find(canFire > 0.5),s_step) = p_firingValue;
    output(find(canFire < 0.5),s_step) = p_notFiringValue;
    activation(find(output(:,s_step) > 0.5), s_step) = 0;
 end

% NOISE ------------------------------------------------------------------%
output(:,s_step) = output(:,s_step) ...
    + ( (rand(stream, p_nr_outs, 1) .* 2 .* p_transmissionNoise) - p_transmissionNoise);
