% This file contains the code that moves the objects around the arena
% during simulation


% for i = 1:nr_A_objs
%    moveOK = 1;
%
%    if (deltaxA(i) == 0)
%       deltaxA(i) =  rand(stream) * p_maxObjSpeed - p_maxObjSpeed/2;
%       deltayA(i) =  rand(stream) * p_maxObjSpeed - p_maxObjSpeed/2;
%    end
%    newxA(i) = xA(i) + deltaxA(i);
%    newyA(i) = yA(i) + deltayA(i);
%
%    if newxA(i) >= 1-1.5* p_sensorRange || newxA(i) <= 0+1.5* p_sensorRange || newyA(i) >= 1-1.5* p_sensorRange || newyA(i) <= 0+1.5* p_sensorRange
%        moveOK = 0;
%    end
%    for j = 1:nr_B_objs
%        if (sqrt( (newxA(i) - xB(j))^2 + (newyA(i) - yB(j))^2) < 1.5* p_sensorRange)
%            moveOK = 0;
%        end
%    end
%    for j = 1:nr_A_objs
%        if (i ~= j)
%        if (sqrt( (newxA(i) - xA(j))^2 + (newyA(i) - yA(j))^2) < 1.5*p_sensorRange)
%            moveOK = 0;
%        end
%        end
%    end
%
%    if moveOK == 1
%        xA(i) = newxA(i);
%        yA(i) = newyA(i);
%    else
%        deltaxA(i) =  rand(stream) * p_maxObjSpeed - p_maxObjSpeed/2;
%        deltayA(i) =  rand(stream) * p_maxObjSpeed - p_maxObjSpeed/2;
%    end
%    if p_visual == 1
%        set(objA(i), 'Position',[xA(i)- p_objDrawRadius yA(i)-p_objDrawRadius 2 * p_objDrawRadius 2 * p_objDrawRadius]);
%    end
% end
%
%
% for i = 1:nr_B_objs
%     moveOK = 1;
%    if (deltayB(i) == 0)
%       deltaxB(i) =  rand(stream) * p_maxObjSpeed - p_maxObjSpeed/2;
%       deltayB(i) =  rand(stream) * p_maxObjSpeed - p_maxObjSpeed/2;
%    end
%    newxB(i) = xB(i) + deltaxB(i);
%    newyB(i) = yB(i) + deltayB(i);
%    if newxB(i) >= 1-p_sensorRange || newxB(i) <= 1.5*p_sensorRange || newyB(i) >= 1-1.5*p_sensorRange || newyB(i) <= 1.5*p_sensorRange
%        moveOK = 0;
%    end
%    for j = 1:nr_A_objs
%        if (sqrt( (xA(j) - newxB(i))^2 + (yA(j) - newyB(i))^2) < 1.5*p_sensorRange)
%            moveOK = 0;
%        end
%    end
%    for j = 1:nr_B_objs
%        if (i ~= j)
%        if (sqrt( (xB(j) - newxB(i))^2 + (yB(j) - newyB(i))^2) < 1.5*p_sensorRange)
%            moveOK = 0;
%        end
%        end
%    end
%
%    if moveOK == 1
%        xB(i) = newxB(i);
%        yB(i) = newyB(i);
%    else
%       deltaxB(i) =  rand(stream) * p_maxObjSpeed - p_maxObjSpeed/2;
%       deltayB(i) =  rand(stream) * p_maxObjSpeed - p_maxObjSpeed/2;
%    end
%    if p_visual == 1
%       set(objB(i), 'Position',[xB(i)-p_objDrawRadius yB(i)-p_objDrawRadius 2 * p_objDrawRadius 2 * p_objDrawRadius]);
%    end
% end

%Alog(:,:,s_step) = [xA; yA];
%Blog(:,:,s_step) = [xB; yB];
