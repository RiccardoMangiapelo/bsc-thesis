disp('Simulation parameters');

fprintf('\n * To run more lifetimes and have automatic policy switch select visualization off');
fprintf('\n * To run a slow and interactive simulation to observe \n   the behaviour, select visualization on\n\n');
answer = input(['Visualisation: off (0), on (1) [default ' num2str(p_visual) ']: ']);
if ~isempty(answer)
    if answer >= 0
        p_visual = answer;
    end
end

if p_visual == 1
    p_totLives = 1;
    p_cycleLength = 100000;
    p_policySwitch = 1;
    modulationPolicy = 1;
    % disp(['p_totLives' num2str(p_totLives)]);
    % disp(['p_cycleLength' num2str(p_cycleLength)]);
else
    disp('You have selected the non-visual simulation. ');
    % disp('- To obtain faster result, try 20 lives instead of 200.');
    % disp('- To plot the temperature graph in Fig. 10D-E choose in the following options');
    % disp('1 Life, a high number of steps, i.e. 100,000 and no policy switch.');

    answer = input(['Lives: [default ' num2str(p_totLives) ']: ']);
    if ~isempty(answer)
        if answer > 0
            p_totLives = answer;
        end
    end
    %disp(['Executing ' num2str(p_totLives) ' lives']);
    %disp('');

    answer = input(['Steps per lifetime: [default ' num2str(p_cycleLength) ']: ']);
    if ~isempty(answer)
        if answer > 2 %&& answer < 2400000 % maximum cap (2400000) removed temporarly. TO BE RESTORED! (probably a different value)
            p_cycleLength = answer;
        end
    end
    %disp(['Lifetime is ' num2str(p_cycleLength) ' simulation steps']);

    p_STEPS = p_totLives * p_cycleLength;            % Total simulation steps

    answer = input(['Policy switch? (no/yes)(0/1): [default ' num2str(p_policySwitch) ']: ']);
    if ~isempty(answer)
        if answer == 0 || answer == 1
            p_policySwitch = answer;
        end
    end

    modulationPolicy = 1;
    answer = input(['Initial policy (1/2)[default ' num2str(modulationPolicy) ']: ']);
    if ~isempty(answer)
        if answer == 1 || answer == 2
            modulationPolicy = answer;
        end
    end

    % answer = input(['Predictors? (off/on)(0/1): [default ' num2str(p_predictorsOnOff) ']: ']);
    % if ~isempty(answer)
    %     if answer == 0 || answer == 1
    %         p_predictorsOnOff = answer;
    %     end
    % end
    % fprintf(['Predictors is set to (0:off/ 1:on):' num2str(p_predictorsOnOff) '. \n This can be changed by changing p_predictorsOnOff in arenaSettings.m' ]);

    % answer = input(['Move objects? (no/yes)(0/1): [default ' num2str(p_objectMoves) ']: ']);
    % if ~isempty(answer)
    %     if answer == 0 || answer == 1
    %         p_objectMoves = answer;
    %     end
    % end
    % disp(['Moving objects is ' num2str(p_objectMoves)]);

end
