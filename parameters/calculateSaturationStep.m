% plot histogram to show when how long it takes for agent to learn WALL, A, B at every simulation

upperSaturation = 9.7;
lowerSaturation = 0.3;

previousReset = lastResetStep(end-1);

GLU_W_L  = GLU_w(1,previousReset:s_step);%reshape(GLU_w(1,:,:),1,size(GLU_w,3));
GABA_W_L = GABA_w(1,previousReset:s_step);%reshape(GABA_w(1,:,:),1,size(GABA_w,3));
GLU_W_R  = GLU_w(2,previousReset:s_step);%reshape(GLU_w(2,:,:),1,size(GLU_w,3));
GABA_W_R = GABA_w(2,previousReset:s_step);%reshape(GABA_w(2,:,:),1,size(GABA_w,3));
GLU_A_L  = GLU_w(3,previousReset:s_step);%reshape(GLU_w(3,:,:),1,size(GLU_w,3));
GABA_A_L = GABA_w(3,previousReset:s_step);%reshape(GABA_w(3,:,:),1,size(GABA_w,3));
GLU_A_R  = GLU_w(4,previousReset:s_step);%reshape(GLU_w(4,:,:),1,size(GLU_w,3));
GABA_A_R = GABA_w(4,previousReset:s_step);%reshape(GABA_w(4,:,:),1,size(GABA_w,3));
GLU_B_L  = GLU_w(5,previousReset:s_step);%reshape(GLU_w(5,:,:),1,size(GLU_w,3));
GABA_B_L = GABA_w(5,previousReset:s_step);%reshape(GABA_w(5,:,:),1,size(GABA_w,3));
GLU_B_R  = GLU_w(6,previousReset:s_step);%reshape(GLU_w(6,:,:),1,size(GLU_w,3));
GABA_B_R = GABA_w(6,previousReset:s_step);%reshape(GABA_w(6,:,:),1,size(GABA_w,3));

previousResetPlus1 = previousReset + 1;
for i=previousResetPlus1:s_step%size(GLU_W_L,3)
  n = i - previousReset;
  if (GLU_W_L(n) > upperSaturation & GABA_W_R(n) > upperSaturation & GLU_A_L(n) > upperSaturation & GABA_A_R(n) > upperSaturation & GABA_B_L(n) > upperSaturation & GLU_B_R(n) > upperSaturation)
  % we learnt the upper correlation
    if (GABA_W_L(n) < lowerSaturation & GABA_A_L(n) < lowerSaturation & GABA_B_R(n) < lowerSaturation & GLU_W_R(n) < lowerSaturation & GLU_A_R(n) < lowerSaturation & GLU_B_L(n) < lowerSaturation)
      % we learnt the lower correlation
       if (correlationLearntAt(end) == 1)
         correlationLearntAt(end) = n;
       else
          correlationLearntAt(end+1) = n;
       end
       break;
    end % if lower
  end % if upper
end % for inside else


% pS = previousReset+1;
% while (pS < size(GLU_W_L,3))
%   if (GLU_W_L(:,:,pS) > upperSaturation & GABA_W_R(:,:,pS) > upperSaturation & GLU_A_L(:,:,pS) > upperSaturation & GABA_A_R(:,:,pS) > upperSaturation & GABA_B_L(:,:,pS) > upperSaturation & GLU_B_R(:,:,pS) > upperSaturation)
%   % we learnt the upper correlation
%     if (GABA_W_L(:,:,pS) < lowerSaturation & GABA_A_L(:,:,pS) < lowerSaturation & GABA_B_R(:,:,pS) < lowerSaturation & GLU_W_R(:,:,pS) < lowerSaturation & GLU_A_R(:,:,pS) < lowerSaturation & GLU_B_L(:,:,pS) < lowerSaturation)
%       % we learnt the lower correlation
%        if (correlationLearntAt(end) == 1)
%          correlationLearntAt(end) = pS - previousReset;
%        else
%           correlationLearntAt(end+1) = pS - previousReset;
%        end
%        break;
%     end % if lower
%   end % if upper
%   pS = pS + 1;
% end % while

% clearing variable
clear GLU_W_L;
clear GABA_W_L;
clear GLU_W_R;
clear GABA_W_R;
clear GLU_A_L;
clear GABA_A_L;
clear GLU_A_R;
clear GABA_A_R;
clear GLU_B_L;
clear GABA_B_L;
clear GLU_B_R;
clear GABA_B_R;

stepsBetweenEvent;
