% old approach

nr_A_objs = 6;
nr_B_objs = 6;
nr_C_objs = 18;%8;
nr_D_objs = 18;%8;
nr_E_objs = 3;%8;
nr_F_objs = 3;%8;
nr_G_objs = 3;%8;
nr_H_objs = 3;%8;

min_width_height          = 0.05;
max_height                = 1 - min_width_height;
max_width                 = p_arenaWidth - min_width_height;
array_letters             = ['A','B','C','D','E','F','G','H'];
existing_x                = [0];
existing_y                = [0];
distance_between_objects  = 0.12;%6;%0.045;
rng('shuffle')      % creates a different seed each time

for i=1:length(array_letters)
  nr_current_obj = 0;
  switch array_letters(i)
    case 'A'
        nr_current_obj = nr_A_objs;
    case 'B'
        nr_current_obj = nr_B_objs;
    case 'C'
        nr_current_obj = nr_C_objs;
    case 'D'
        nr_current_obj = nr_D_objs;
    case 'E'
        nr_current_obj = nr_E_objs;
    case 'F'
        nr_current_obj = nr_F_objs;
    case 'G'
        nr_current_obj = nr_G_objs;
    case 'H'
        nr_current_obj = nr_H_objs;
    otherwise
        nr_current_obj = 0;
  end % of switch

  for c=1:nr_current_obj
    random_x = rand() * (max_width-min_width_height) + min_width_height;
    random_y = rand() * (max_height-min_width_height) + min_width_height;
    k = 1;
    while ( k<=length(existing_x) )
      if abs( random_x - existing_x(k) ) <= distance_between_objects & ...
         abs( random_y - existing_y(k) ) <= distance_between_objects
        random_x = rand() * (max_width-min_width_height) + min_width_height;
        random_y = rand() * (max_height-min_width_height) + min_width_height;
        k = 1;
      else
        k = k+1;
      end

    end
    existing_x(end+1)= random_x;
    existing_y(end+1)= random_y;

    eval(['x' array_letters(i) '(c)' '=random_x;']);
    eval(['y' array_letters(i) '(c)' '=random_y;']);
  end % of c loop

end % of i loop
