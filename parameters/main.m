

% Draw static parts of the arena
if p_visual == 1
    drawArena;
end


if p_visual == 0
  drawArena;
  defineObjectsAppearance;
  disp(sprintf ('\nPress a Enter to start the simulation'));
  % input('Press Enter to start the simulation');
  pause;
  % waitforbuttonpress;
end

tic;

% figTraces = figure('Name','Eligibility traces');

while (s_step < p_STEPS) && (simulationEnded == 0)

    % During simulation, some changes (e.g the modulation policy) can be
    % enforced by adding conditions in this file
    onlineChanges;
    % Read all sensory information
    readSensorsInArena;
    % Compute output according to sensory information
    neuralComputation;
    % Move the agent in the environment according to the neural output, and
    % assign modulation values
    agentMoves;

    % Riccardo:
    % agentMoves has to happen before because in there the modulation is calculated
    % so first we calculate the modulation in agentMoves, then we store it in FIFO
    %
    % 1) call the script that triggers the FIFO (and successively the thresholds)
    storeInFIFO;
    % 2) call the script with the autoencoder
    autoencoderComponent;
    % 3) then call the script that updates the traces and assign the reward
    updateTraces;

    % Compute plasticity according to neural activity and modulation value
    plasticity;


    % Redraw dynamic parts of the arena
    if (p_visual == 1)
        if (mod(s_step,p_plotEveryNrSteps) == 0)
            drawArenaRefresh;
        end
    else
        if mod(s_step,round(p_STEPS/20)) == 0
            disp([num2str(round(s_step/p_STEPS * 100)) '% completed']);
        end
    end

% ----------------------------
    %if (mod(s_step,10) == 0)
    % g_one = [g_one x0];%sensorC(1)];
    % g_two = [g_two y0];%sensorC(2)];
    % conc_of_shortTermHebb(:,s_step)= shortTermHebb - HebbTerm;
    conc_TraceB(:,s_step) = shortTermHebb - HebbTerm;
    % conc_of_steerIntensity(:,s_step)= steerIntensity;
    % conc_of_sumOfOutput(:,s_step)= sumOfOutput;
    %
    % plotTraces;
    % plotSteerIntensity;
    %end
% ----------------------------

    % stepsBetweenEvent;
    % Increment simulation step by 1
    s_step = s_step + 1;

end
