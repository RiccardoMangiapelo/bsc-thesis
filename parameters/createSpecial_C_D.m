for i=1:length(array_letters)
  nr_current_obj = 0;
  obj_around_current_obj = 0;
  switch array_letters(i)
    case 'C'
        nr_current_obj = nr_C_objs;
    case 'D'
        nr_current_obj = nr_D_objs;
    otherwise
        nr_current_obj = 0;
  end % of switch


  for c=1:nr_current_obj

    if (array_letters(i) == 'C')

      flag = false;
      [receiver_xA,idx] = datasample(xA,1); %picks a random value in the array and stores it in receiver_xA and its index in idx
      % if the number of indexes in array is less than maxNumb_ofPredictors (i.e. we used this index more less maxNumb_ofPredictors times)
      while (~flag) % until it is true
        if (numel(find(idx == how_many_times_xA_used)) < maxNumb_ofPredictors) % how many times idx is present in the array
          how_many_times_xA_used(end+1) = idx;
          flag = true;
        else
          [receiver_xA,idx] = datasample(xA,1);
        end
      end
      receiver_yA = yA(idx); % get the equivalent y value of the randomly picked x above

        random_x = rand() * ((receiver_xA + angel_devil_area) - (receiver_xA - angel_devil_area)) + (receiver_xA - angel_devil_area); % random number in the range of beginning and end of devil area
        random_y = rand() * ((receiver_yA + angel_devil_area) - (receiver_yA - angel_devil_area)) + (receiver_yA - angel_devil_area);
        k = 1;
        while ( k<=length(existing_x) )

          if (abs( random_x - existing_x(k) ) <= distance_within_vip &&...
             abs( random_y - existing_y(k) ) <= distance_within_vip) || ...
             ~(random_x > min_width_height && random_x < max_width) || ...
             ~(random_y > min_width_height && random_y < max_height)
                  random_x = rand() * ((receiver_xA + angel_devil_area) - (receiver_xA - angel_devil_area)) + (receiver_xA - angel_devil_area);
                  random_y = rand() * ((receiver_yA + angel_devil_area) - (receiver_yA - angel_devil_area)) + (receiver_yA - angel_devil_area);
                  k = 1;
          else
                  k = k+1;
          end

        end
        existing_x(end+1)= random_x;
        existing_y(end+1)= random_y;

        xC(c) = random_x;
        yC(c) = random_y;


      elseif (array_letters(i) == 'D')

        flag = false;
        [receiver_xB,idx] = datasample(xB,1); %picks a random value in the array and stores it in receiver_xA and its index in idx
        % if the number of indexes in array is less than maxNumb_ofPredictors (i.e. we used this index more less maxNumb_ofPredictors times)
        while (~flag) % until it is true
          if (numel(find(idx == how_many_times_xB_used)) < maxNumb_ofPredictors) % how many times idx is present in the array
            how_many_times_xB_used(end+1) = idx;
            flag = true;
          else
            [receiver_xB,idx] = datasample(xB,1);
          end
        end
        receiver_yB = yB(idx); % get the equivalent y value of the randomly picked x above

          random_x = rand() * ((receiver_xB + angel_devil_area) - (receiver_xB - angel_devil_area)) + (receiver_xB - angel_devil_area); % random number in the range of beginning and end of devil area
          random_y = rand() * ((receiver_yB + angel_devil_area) - (receiver_yB - angel_devil_area)) + (receiver_yB - angel_devil_area);
          k = 1;
          while ( k<=length(existing_x) )

            if (abs( random_x - existing_x(k) ) <= distance_within_vip && ...
               abs( random_y - existing_y(k) ) <= distance_within_vip) || ...
               ~(random_x > min_width_height && random_x < max_width) || ...
               ~(random_y > min_width_height && random_y < max_height)
                    random_x = rand() * ((receiver_xB + angel_devil_area) - (receiver_xB - angel_devil_area)) + (receiver_xB - angel_devil_area);
                    random_y = rand() * ((receiver_yB + angel_devil_area) - (receiver_yB - angel_devil_area)) + (receiver_yB - angel_devil_area);
                    k = 1;
            else
                    k = k+1;
            end

          end
          existing_x(end+1)= random_x;
          existing_y(end+1)= random_y;

          xD(c) = random_x;
          yD(c) = random_y;

      end


  end % of c loop

end % of i loop
