
% gradualita di aggiornamento pesi  % Different seedNumbers from the launch-files can be used                                                                           % to simulate different pseudo-noise sequences
p_weightUpdateRate    = 0.02;  % update rate of weights: must be chosen in relation to the weigh maximum
% update samplingTime when
% updating p_weightUpdateRate

p_maxWeight           = 10;    % Maxium weight value at which saturation occurs

p_transmissionNoise   = 0.1;%p_weightUpdateRate/10;%0.001 seems ok for 0.003;%0.1;   % Noise in neural transmission

p_useSpikes           = 0;     % navigation is tested here
                               % with the rate based model

p_useTanh             = 1;     % when useSig is 0 tanh is used, otherwise the sigmoid function is used
                               % tanh is used instead of sigmoid

p_increaseStepsBy     = 1;     % to be used in spiking simulation for increasing the simulation steps

p_probSpike           = 0.5;   % Probability of emitting a spike for a fully active neuron when using the spiking model

p_tau                 = 1;     % Leaky integrator time-constant (integration step = 1)
                               % tau 1 means that the neuron doesn't
                               % have a continuous time dynamic
                               % (no memory of the past)

p_notFiringValue      = -0.1;  % Neuron output when not firing (this is not zero to allow Hebbian
                               % computation when the neuron is not firing.
p_firingValue         = 1;

p_nr_ins              = 18;    % number of inputs of the network

p_nr_outs             = 1;     % number of outputs of the network

p_randomReset         = 0;     % if 0 the reset of weights is to maxWeight/2.
                               % If 1 the reset of the weights is random uniform in [0 MaxWeights]
