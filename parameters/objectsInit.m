% OBJECTS TYPES, NUMBER AND POSITION

% Number of objects for each type

nr_A_objs = 18;
nr_B_objs = 18;
nr_C_objs = 48;
nr_D_objs = 48;
nr_E_objs = 30;
nr_F_objs = 30;
nr_G_objs = 30;
nr_H_objs = 30;


min_width_height          = 0.05;
max_height                = 1 - min_width_height;
max_width                 = p_arenaWidth - min_width_height;
array_letters             = ['A','B','C','D','E','F','G','H'];
existing_x                = [0];
existing_y                = [0];

tenSecs                   = 0.025;  % 10 sec
distance_between_objects  = 0.0625; % 25 sec
distance_within_vip       = 0.042;  % 16 sec
angel_devil_area          = 0.045;  % 18 sec    % area around positive_negative objects
BA_dist                   = 0.045;
extra_distance_between_goodAndBad  = 0.023;

how_many_times_xA_used    = [0]; % this stores the index of the random picked xA in order to compare every time how many times the randomly picked value has been used
how_many_times_xB_used    = [0]; % this stores the index of the random picked xB in order to compare every time how many times the randomly picked value has been used
far_away_value            = 0;
xA                        = [];
xB                        = [];
min_width_heightAB        = min_width_height + angel_devil_area;
max_heightAB              = max_height - angel_devil_area;
max_widthAB               = max_width - angel_devil_area;
maxNumb_ofPredictors = 3;

if p_predictorsOnOff == 2
  answer = input(['Path to file: ']);
  if ~isempty(answer)
     fromWhere = answer;
  end
  load(fromWhere)
  createSpecial_C_D;

elseif p_predictorsOnOff == 1
  % create objs randomly
  createRandomObjects;

  % save position of A B E F G H
  % on my computer
    saveLocationName = '/Users/riccardo/Desktop/lastExperimentObjPosition.mat';
  % on alioth
    % saveLocationName = 'C:\Users\localadmin\Documents\Riccardo_Mangiapelo_FYP\Simulations\lastExperimentObjPosition.mat'; %lives_',num2str(p_totLives),'_steps_',num2str(p_cycleLength),'_policy_',num2str(p_policySwitch),'-',num2str(modulationPolicy));
  % on dale-research
    % saveLocationName = '/home/dale-research/Documents/Riccardo_Mangiapelo_FYP/Simulations/lastExperimentObjPosition.mat'; %lives_',num2str(p_totLives),'_steps_',num2str(p_cycleLength),'_policy_',num2str(p_policySwitch),'-',num2str(modulationPolicy));
    save(saveLocationName, 'xA','yA','xB','yB','xC','yC','xD','yD','xE','yE','xF','yF','xG','yG','xH','yH');

elseif p_predictorsOnOff == 0
 % import position of A B E F G H
 % on my computer
    fromWhere = '/Users/riccardo/Desktop/lastExperimentObjPosition.mat';
 % on alioth
    % fromWhere = 'C:\Users\localadmin\Documents\Riccardo_Mangiapelo_FYP\Simulations\lastExperimentObjPosition';%strcat('C:\Users\localadmin\Documents\Riccardo_Mangiapelo_FYP\Simulations\lives_',num2str(p_totLives),'_steps_',num2str(p_cycleLength),'_policy_',num2str(p_policySwitch),'-',num2str(modulationPolicy));
 % on dale-research
      % fromWhere = '/home/dale-research/Documents/Riccardo_Mangiapelo_FYP/Simulations/lastExperimentObjPosition';%strcat('C:\Users\localadmin\Documents\Riccardo_Mangiapelo_FYP\Simulations\lives_',num2str(p_totLives),'_steps_',num2str(p_cycleLength),'_policy_',num2str(p_policySwitch),'-',num2str(modulationPolicy));
 load(fromWhere,'xA','yA','xB','yB','xE','yE','xF','yF','xG','yG','xH','yH');
 % randomly position C D
 createRandom_C_D;
end
