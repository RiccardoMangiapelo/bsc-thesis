%
figTraces = figure('Name','Eligibility traces');


figure(figTraces);

%event happened
% subplot(3,1,1);
% plot(neg_event(1:s_step),'r')
% hold on
% plot(pos_event(1:s_step),'b')
% hold off
% ylabel('Event Happened');


% CORRECT PLOTTING:
    %trace
    % ax1 = subplot(2,1,1);
    % abs because otherwise some are positive some are negative.
    % The reason being that some are GABA, some GLU, so the opposite sensor for the same object
    plot(abs(conc_of_shortTermHebb(:,1:s_step)'),'LineWidth',1);
    grid on;
    % [xmin,xmax,ymin,ymax]
    % axis([0,s_step,0,0.2]);
    % legend(ax1, 'Wall l','Wall r','A l','A r','B l','B r','C l','C r','D l','D r','E l','E r','F l','F r','G l','G r','H l','H r');
    ylabel('Trace');
    % name = strcat('Eligibility traces.fig');
    % saveas(figTraces,fullfile(filePath,name));


    %Event happened + sizeOfBuffer
    % neg_eventSOB = [SOB_zeros, neg_event];
    % pos_eventSOB = [SOB_zeros, pos_event];
    % ax2 = subplot(2,1,2);
    % plot(neg_eventSOB(1:s_step),'r')
    % hold on
    % plot(pos_eventSOB(1:s_step),'b')
    % hold off
    % ylabel('Multiplication happens (event + sob)');
    % plot (reshape(modHebbTerm(:,1,1:s_step),18,s_step)')
    % [xmin,xmax,ymin,ymax]
    % axis([0,s_step,-1,1]);
    % legend(ax2, 'Wall l','Wall r','A l','A r','B l','B r','C l','C r','D l','D r','E l','E r','F l','F r','G l','G r','H l','H r');
    % ylabel('modHebbTerm');
