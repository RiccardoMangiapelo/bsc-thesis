if p_randomReset == 0
    GLU_w(:,s_step) = ones(p_nr_ins, p_nr_outs) * p_maxWeight / 2;
    GABA_w(:,s_step) = ones(p_nr_ins, p_nr_outs) * p_maxWeight / 2;
    GLU_w(:,s_step+1) = ones(p_nr_ins, p_nr_outs) * p_maxWeight / 2;
    GABA_w(:,s_step+1) = ones(p_nr_ins, p_nr_outs) * p_maxWeight / 2;
else
    GLU_w(:,s_step) = ones(p_nr_ins,p_nr_outs) .* p_maxWeight .* rand(stream, p_nr_ins, p_nr_outs);
    GABA_w(:,s_step) = ones(p_nr_ins,p_nr_outs) .* p_maxWeight .* rand(stream, p_nr_ins, p_nr_outs);
    GLU_w(:,s_step+1) = ones(p_nr_ins,p_nr_outs) .* p_maxWeight .* rand(stream, p_nr_ins, p_nr_outs);
    GABA_w(:,s_step+1) = ones(p_nr_ins,p_nr_outs) .* p_maxWeight .* rand(stream, p_nr_ins, p_nr_outs);
end

elapsedTime = toc;
% disp(['Weights reset at step: ' num2str(s_step) ' (Comp time since last reset: ' num2str(elapsedTime) ')']);
tic;
