startFrom = 1;
stopAt = size(log_XYMod,2);

% Arena
f2 = figure('Position',[0 0 500 500], 'Resize', 'off', 'DockControls','off','Name','Agent path');
figure(f2);
rectangle('Position',[-.01,-.01,p_arenaWidth+.01,1+.01],'facecolor','w');

axis fill;
axis off;
axis equal;
axis manual;


pause(1);

stream = RandStream('mrg32k3a', 'Seed', 1);

placeObjectsOnWindow;
defineObjectsAppearance;

% for i = startFrom:stopAt
%         rectangle('Position',[log_XYMod(1,i)- .002,log_XYMod(2,i)- .002, 2* .002, 2 * .002],...
%             'Curvature',[1,1], 'facecolor',[0.0 0.0 0.0],'LineStyle','none');
% end


name = strcat('Agent Path.fig');
saveas(f2,fullfile(filePath,name));
