figHandle1 = figure('Name','Weights');
figHandle2 = figure('Name','Weights');
figHandle3 = figure('Name','Weights');

% plot(g_one,'.-')
% hold on
% plot(g_two,'.-')
% hold off
% ylabel('X0 = red');

% plot(conc_of_shortTermHebb(p_nr_ins,1:s_step));

        % subplot(3,1,3);
        % plot (reshape(modHebbTerm(:,1,1:s_step),6,s_step)')
        % ylabel('modHebbTerm');

        % if I move the number of output to the third dimension, so the third dimension is 1 and is fixed, then
        % plot (modHebbTerm(:,1:s_step,1))

        % Plotting GABA and GLU
      figure(figHandle1);
        for i=1:6
            subplot(2,3,i);
            plot(reshape(GABA_w(i,1,1:s_step),1,s_step),'b')
            hold on
            plot(reshape(GLU_w(i,1,1:s_step),1,s_step),'r')
            hold off
            axis([0,s_step,0,10]);
            if i==1 ylabel('Wall Left'); end
            if i==2 ylabel('Wall Right'); end
            if i==3 ylabel('A Left'); end
            if i==4 ylabel('A Right'); end
            if i==5 ylabel('B Left'); end
            if i==6 ylabel('B Right'); end
        end


    figure(figHandle2);
        % Plotting GABA and GLU
        flag=true; % k is set to 7
        k = 7;
        % flag=false; % k is set to 13
        % k = 13;
        for i=k:(k+5)
            if flag
              subplot(2,3,(i-6));
            else
              subplot(2,3,(i-12));
            end

            plot(reshape(GABA_w(i,1,1:s_step),1,s_step),'b')
            hold on
            plot(reshape(GLU_w(i,1,1:s_step),1,s_step),'r')
            hold off
            axis([0,s_step,0,10]);
            if k==1 ylabel('Wall Left'); end
            if k==2 ylabel('Wall Right'); end
            if k==3 ylabel('A Left'); end
            if k==4 ylabel('A Right'); end
            if k==5 ylabel('B Left'); end
            if k==6 ylabel('B Right'); end
            if k==7 ylabel('C Left'); end
            if k==8 ylabel('C Right'); end
            if k==9 ylabel('D Left'); end
            if k==10 ylabel('D Right'); end
            if k==11 ylabel('E Left'); end
            if k==12 ylabel('E Right'); end
            if k==13 ylabel('F Left'); end
            if k==14 ylabel('F Right'); end
            if k==15 ylabel('G Left'); end
            if k==16 ylabel('G Right'); end
            if k==17 ylabel('H Left'); end
            if k==18 ylabel('H Right'); end

            k = k+1;
        end

        figure(figHandle3);
        % Plotting GABA and GLU
          % flag=true; % k is set to 7
          % k = 7;
          flag=false; % k is set to 13
          k = 13;
          for i=k:(k+5)
              if flag
                subplot(2,3,(i-6));
              else
                subplot(2,3,(i-12));
              end

              plot(reshape(GABA_w(i,1,1:s_step),1,s_step),'b')
              hold on
              plot(reshape(GLU_w(i,1,1:s_step),1,s_step),'r')
              hold off
              axis([0,s_step,0,10]);
              if k==1 ylabel('Wall Left'); end
              if k==2 ylabel('Wall Right'); end
              if k==3 ylabel('A Left'); end
              if k==4 ylabel('A Right'); end
              if k==5 ylabel('B Left'); end
              if k==6 ylabel('B Right'); end
              if k==7 ylabel('C Left'); end
              if k==8 ylabel('C Right'); end
              if k==9 ylabel('D Left'); end
              if k==10 ylabel('D Right'); end
              if k==11 ylabel('E Left'); end
              if k==12 ylabel('E Right'); end
              if k==13 ylabel('F Left'); end
              if k==14 ylabel('F Right'); end
              if k==15 ylabel('G Left'); end
              if k==16 ylabel('G Right'); end
              if k==17 ylabel('H Left'); end
              if k==18 ylabel('H Right'); end

              k = k+1;
          end


          name = strcat('Weights NOT CLEAR 1.fig');
          saveas(figHandle1,fullfile(filePath,name));


          name = strcat('Weights NOT CLEAR 2.fig');
          saveas(figHandle2,fullfile(filePath,name));

          name = strcat('Weights NOT CLEAR 3.fig');
          saveas(figHandle3,fullfile(filePath,name));
