% this script:
%   1) stores the modulation in the FIFO buffer
%   2) stores the most novel experience in the novelty-RAAHN buffer
%   3) successively it will apply the thresholds as well

% Hebbian Modulation

randomNumber = randi([1,sizeOfBuffer]);

fifo_buffer(2:sizeOfBuffer) = fifo_buffer(1:sizeOfBuffer-1);

% fifo_buffer(1) =  modulation;

% add the modulation to the current value of fifo_buffer(randomNumber)
if modulation > 25 | modulation < -25
  modulation = 0;
end
%
fifo_buffer(1) = 0;
fifo_buffer(randomNumber) = fifo_buffer(randomNumber) + modulation;
% fifo_buffer(randomNumber) =  modulation;

% novelty-RAAHN Buffer
% only the most novel experiences encountered during the agent's lifetime are stored in the buffer


% 1) calculate EUCLIDEAN DISTANCE between
% %    new experience and all other experiences in buffer
%
% % calculating euclidian distance
% euclideanDist = dist(novelty_buffer,modulation);
% % storing euclidian distance in a fifo like buffer
% euclidian_buffer(2:sizeOfEucledianBuffer) = euclidian_buffer(1:sizeOfEucledianBuffer-1);
% euclidian_buffer(1) = euclideanDist;
%
% % 2) assign new experience with a novelty score,
% %    which is the sum of the 20 smallest such distances
% sorted_buffer = sort(euclidian_buffer);
% noveltyScore_newExperience = sum(sorted_buffer(1:20));
%
% % 3) if new_experience_novelty_score > novelty_score_of_least_novel_experience_in_buffer
% %                 A (new)                             B (old)
% %    then replace B (old) with A (new)
% if (noveltyScore_newExperience > novelty_buffer(leastNovel))
%   novelty_buffer(leastNovel) = noveltyScore_newExperience;
%   leastNovel = leastNovel-1;
%   if (leastNovel < 1)
%     leastNovel = sizeOfNoveltyBuffer;
%   end
% end
