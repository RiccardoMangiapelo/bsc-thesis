% plotObjTraceWeight

figObjTraceWeight = figure('Name','Reward, trace and weight update');
figure(figObjTraceWeight);

ax1 = subplot(4,1,1);
% when the object is eaten and when the reward is given on a line
until = correlationLearntAt(2);
rew = rewardAtStep(1:until);
B_objINDEX = find(B_happened < until);
B_obj = B_happened(B_objINDEX);
plot(B_obj,ones(length(B_obj),1),'.r');
hold on
plot(rew,ones(length(rew),1),'.b');
hold off
ylabel(ax1, 'Object/Reward');
legend(ax1, 'Object', 'Reward');

ax2 = subplot(4,1,2);
% sensor(input) and output as 2 lines
plot(sensorBconc6(1:until),'b')
hold on
plot(output(6,1:until),'r')
hold off
ylabel(ax2, 'Input/Output');
legend(ax2, 'Input', 'Output');

ax3 = subplot(4,1,3);
% trace of that particular object
plot(abs(conc_TraceB(6,1:until)'),'LineWidth',1);
grid on;
ylabel(ax3, 'Trace');
legend(ax3, 'Type-B object');

ax4 = subplot(4,1,4);
% weight update
% 6 == B right GlU, which gets reinforced and grows to 10
plot(GLU_w(6,1:until));
ylabel(ax4, 'Weight update');
legend(ax4, 'Type-B weight');



name = strcat('Reward trace and weight update.fig');
saveas(figObjTraceWeight,fullfile(filePath,name));
