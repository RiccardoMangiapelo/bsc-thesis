% % NONE OF THE FOLLOWING VARIABLES HAS BEEN INITIALISED!!
%
% % Forward activation Aj         eq(2) - RAAHN 2016
% forwardActivationGABA = tanh (novelty_buffer .* GABA_w(:,s_step));
% forwardActivationGLU  = tanh (novelty_buffer .* GLU_w(:,s_step));
%
% % Backward activation Bi        eq(3) - RAAHN 2016
% backwardActivationGABA  = tanh (forwardActivationGABA .* GABA_w(:,s_step));
% backwardActivationGLU   = tanh (forwardActivationGLU .* GLU_w(:,s_step));
%
% % Reconstruction error E        eq(4) - RAAHN 2016
% recErrorGABA  = novelty_buffer - backwardActivationGABA;
% recErrorGLU   = novelty_buffer - backwardActivationGLU;
%
% % Delta i for input neuron i    eq(5) - RAAHN 2016
% delta_I_GABA  = recErrorGABA .* diff(backwardActivationGABA);
% delta_I_GLU   = recErrorGLU .* diff(backwardActivationGLU);
%
% % Delta j for hidden neuron j    eq(6) - RAAHN 2016
% delta_J_GABA  = sum(delta_I_GABA .* GABA_w(:,s_step)) .* diff(forwardActivationGABA);
% delta_J_GLU   = sum(delta_I_GLU .* GLU_w(:,s_step)) .* diff(forwardActivationGLU);
%
% % Weight update
% % I AM MISSING ALPHA α
% GABA_w(:,s_step) = α .* ( (delta_I_GABA .* forwardActivationGABA) + (delta_J_GABA .* novelty_buffer));
% GLU_w(:,s_step) = α .* ( (delta_I_GLU .* forwardActivationGLU) + (delta_J_GLU .* novelty_buffer));
