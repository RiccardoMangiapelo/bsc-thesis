
sampleInterval = 20;

a = squeeze(GLU_w);             % from n x 1 x STEP to n x STEP
b = squeeze(GABA_w);

GLU_w_reshaped = reshape(a,18, p_cycleLength,p_totLives);
GABA_w_reshaped = reshape(b,18, p_cycleLength,p_totLives);

if p_totLives > 1
    data_GLU1 = mean(GLU_w_reshaped(:,:,1:2:p_totLives -1),3)';
    data_GLU2 = mean(GLU_w_reshaped(:,:,2:2:p_totLives),3)';
% Wall, A, B
    createWeightsfigure(data_GLU1(1:sampleInterval:p_cycleLength,1:6),1,0, filePath);
    createWeightsfigure(data_GLU2(1:sampleInterval:p_cycleLength,1:6),2,0, filePath);
% A and C
    createWeightsfigure(data_GLU1(1:sampleInterval:p_cycleLength,[3:4 7:8]),1,1, filePath);
    createWeightsfigure(data_GLU2(1:sampleInterval:p_cycleLength,[3:4 7:8]),2,1, filePath);
% B and D
    createWeightsfigure(data_GLU1(1:sampleInterval:p_cycleLength,[5:6 9:10]),1,2, filePath);
    createWeightsfigure(data_GLU2(1:sampleInterval:p_cycleLength,[5:6 9:10]),2,2, filePath);
% E, F, G, H
    createWeightsfigure(data_GLU1(1:sampleInterval:p_cycleLength,11:18),1,3, filePath);
    createWeightsfigure(data_GLU2(1:sampleInterval:p_cycleLength,11:18),2,3, filePath);


    data_GLU1_aug = [(1:sampleInterval:p_cycleLength)' data_GLU1(1:sampleInterval:p_cycleLength,:)];
    saveTo = strcat(filePath,'/WeightsDataE2odd_forImport.txt');
    % save -ascii -tabs saveTo data_GLU1_aug
    save(saveTo, 'data_GLU1_aug', '-ascii','-tabs');

    data_GLU2_aug = [(1:sampleInterval:p_cycleLength)' data_GLU2(1:sampleInterval:p_cycleLength,:)];
    saveTo = strcat(filePath,'/WeightsDataE2even_forImport.txt');
    % save -ascii -tabs saveTo data_GLU2_aug
    save(saveTo, 'data_GLU2_aug', '-ascii','-tabs');

    % saveas(data_GLU1_aug,fullfile(filePath,'Data Glu'));
    % saveas(data_GLU2_aug,fullfile(filePath,'Data Glu'));

else
    data_GLU1 = GLU_w_reshaped(:,:,1)';

    % Wall, A, B
        createWeightsfigure(data_GLU1(1:sampleInterval:p_cycleLength,1:6),1,0, filePath);
    % A and C
        createWeightsfigure(data_GLU1(1:sampleInterval:p_cycleLength,[3:4 7:8]),1,1, filePath);
    % B and D
        createWeightsfigure(data_GLU1(1:sampleInterval:p_cycleLength,[5:6 9:10]),1,2, filePath);
    % E, F, G, H
        createWeightsfigure(data_GLU1(1:sampleInterval:p_cycleLength,11:18),1,3, filePath);

      data_GLU1_aug = [(1:sampleInterval:p_cycleLength)' data_GLU1(1:sampleInterval:p_cycleLength,:)];
      saveTo = strcat(filePath,'/WeightsDataE2odd_forImport.txt');
      % save -ascii -tabs saveTo data_GLU1_aug
      save(saveTo, 'data_GLU1_aug', '-ascii','-tabs');
end

% clearing variable
clear a;
clear b;
clear sampleInterval;
clear GLU_w_reshaped;
clear GABA_w_reshaped;
clear data_GLU1;
clear data_GLU1_aug;
clear data_GLU2;
clear data_GLU2_aug;
