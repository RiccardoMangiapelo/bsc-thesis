% Agent simulation and modulation

% The turn strength is proportional to the neural output
deltaAngle =  p_maxAngle * p_outputMapping * output(1,s_step);

if (wallContact == 0)
    angle = angle - deltaAngle;
end

if (angle < 0) angle = angle + 2*pi; end
if (angle > 2*pi) angle = angle - 2*pi; end

% Agent moves ------------------------------------------------------------%

    x0 = x0 + (cos(angle) * p_distPerStep);
    y0 = y0 + (sin(angle) * p_distPerStep);

    x0Array(s_step) = x0;
    y0Array(s_step) = y0;

    xL = x0 + ((p_objSensorDistanceFromBody) * cos(angle + p_objSensorAngle));
    yL = y0 + ((p_objSensorDistanceFromBody) * sin(angle + p_objSensorAngle));
    xR = x0 + ((p_objSensorDistanceFromBody) * cos(angle - p_objSensorAngle));
    yR = y0 + ((p_objSensorDistanceFromBody) * sin(angle - p_objSensorAngle));

% Bouncing back from walls -----------------------------------------------%

modulation = 0;

% OBJECT WALL
% approaching the wall
if (distW < 1 * p_sensorRange)
    % approaching is neg modulated, escaping is pos modulated.
     modulation =  (distW - distW_1) / (2*p_distPerStep);
     if modulation > 0 modulation = min(modulation, 1);end
end


wallContact = 0;
if (x0 <= 0) x0 = 0.02; wallContact = 1; end
if (x0 >= p_arenaWidth) x0 = p_arenaWidth - 0.02; wallContact = 1; end
if (y0 <= 0) y0 = 0.02; wallContact = 1; end
if (y0 >= 1) y0 = 1-0.02; wallContact = 1; end

% robot is hitting the wall
if (wallContact == 1)
    modulation = p_negMod;
    all_happened(end+1) = s_step;

    % Repositioning the robot
    % collision and slight rotation
    if sensor(1) > sensor(2)
        angle = angle - pi/6;
    else
        angle = angle + pi/6;
    end

    if (angle < 0) angle = angle + 2*pi;end;
    if (angle > 2*pi) angle = angle - 2*pi;end
end




% Bouncing back from NEUTRAL OBJECT ---------------------------------------%
%
% if (s_step > 60)
%     % calculating the difference between previous values of x0
%     x0_x0Minus1 = abs( x0Array(s_step) - x0Array(s_step-20) );
%     x0Minus1_x0Minus2 = abs( x0Array(s_step-20) - x0Array(s_step-40) );
%     x0Minus2_x0Minus3 = abs( x0Array(s_step-40) - x0Array(s_step-60) );
%
%     % calculating the difference between previous values of y0
%     y0_y0Minus1 = abs( y0Array(s_step) - y0Array(s_step-20) );
%     y0Minus1_y0Minus2 = abs( y0Array(s_step-20) - y0Array(s_step-40) );
%     y0Minus2_y0Minus3 = abs( y0Array(s_step-40) - y0Array(s_step-60) );
%
%     % if all of the values calculated above are less than 0.0020
%     if(  (x0_x0Minus1 < p_distPerStep-0.0005) && ...
%          (x0Minus1_x0Minus2 < p_distPerStep-0.0005) && ...
%          (y0_y0Minus1 < p_distPerStep-0.0005) && ...
%          (y0Minus1_y0Minus2 < p_distPerStep-0.0005)  )
%
%          modulation = p_negMod;
%
%         % % Repositioning the robot. Collision and slight rotation
%         % if ( sensor(7) > sensor(8) )% |  sensor(9) > sensor(10) |  sensor(11) > sensor(12) |  sensor(13) > sensor(14) |  sensor(15) > sensor(16) |  sensor(17) > sensor(18) )
%         %   disp('-');
%         %   disp(s_step);
%         %     angle = angle - pi/4;
%         %     % pause(1)
%         % else
%         %   disp('+');
%         %   disp(s_step);
%         %     angle = angle + pi/4;
%         %     % pause(1)
%         % end
%         %
%         % if (angle < 0) angle = angle + 2*pi;end;
%         % if (angle > 2*pi) angle = angle - 2*pi;end
%
%     end
%   end







% OBJECT A
for i = 1:nr_A_objs
    distA_1(i) = distA(i);
    distA(i) = sqrt( abs( (x0 - xA(i))^2 + (y0 - yA(i))^2));

    if (distA(i) <  p_impactRange)  % object destroyed
        A_OffBoard(i) = 1;
        A_happened(end+1) = s_step;
        all_happened(end+1) = s_step;
        A_happenedCurrentLife(end+1) = s_step;
    end
    if (distA(i) > 2 * p_sensorRange)     % object restored
        A_OffBoard(i) = 0;
    end

    if (modulationPolicy == 1)
        if (distA(i) < 1 * p_sensorRange) && (A_OffBoard(i) == 0)
            % approaching is neg modulated, escaping is pos modulated.
             modulation = ( (distA(i) - distA_1(i))) / p_distPerStep;
           %  modulation = turnStrength;
        end
    else
        if (distA(i) <  1 * p_sensorRange)  && (A_OffBoard(i) == 0);
            modulation = - ( (distA(i) - distA_1(i))) / p_distPerStep;
           % modulation = -turnStrength;
        end
    end

end

% OBJECT B
if (p_nr_ins > 4)
    for i = 1:nr_B_objs
        distB_1(i) = distB(i);
        distB(i) = sqrt( abs( (x0 - xB(i))^2 + (y0 - yB(i))^2));
        if (distB(i) <  p_impactRange)
            % object destroyed
            B_OffBoard(i) = 1;
            B_happened(end+1) = s_step;
            all_happened(end+1) = s_step;
            B_happenedCurrentLife(end+1) = s_step;
        end
        if (distB(i) > 2 * p_sensorRange)
            % object restored
            B_OffBoard(i) = 0;
        end

        if (modulationPolicy == 1)
            if (distB(i) < 1 * p_sensorRange) && (B_OffBoard(i) == 0)
                modulation = - ( (distB(i) - distB_1(i))) / p_distPerStep;
                % modulation = -turnStrength;
            end
        else
            if (distB(i) < 1 * p_sensorRange) && (B_OffBoard(i) == 0)
                modulation = ( (distB(i) - distB_1(i))) / p_distPerStep;
                %modulation = turnStrength;
           end
        end
    end
end

% OBJECT C
if (p_nr_ins > 6)
    for i = 1:nr_C_objs
        distC_1(i) = distC(i);
        distC(i) = sqrt( abs( (x0 - xC(i))^2 + (y0 - yC(i))^2));
        if (distC(i) <  p_impactRange)
            % object destroyed
            C_OffBoard(i) = 1;
            C_happened(end+1) = s_step;
            all_happened(end+1) = s_step;
            C_happenedCurrentLife(end+1) = s_step;
        end
        if (distC(i) > 2 * p_sensorRange)
            % object restored
            C_OffBoard(i) = 0;
        end

        if (modulationPolicy == 1)
            if (distC(i) < 1 * p_sensorRange) && (C_OffBoard(i) == 0)
                % modulation = neutralMod;
            end
        else
            if (distC(i) < 1 * p_sensorRange) && (C_OffBoard(i) == 0)
                % modulation = p_posMod;
           end
        end
    end
end

% OBJECT D
if (p_nr_ins > 8)
    for i = 1:nr_D_objs
        distD_1(i) = distD(i);
        distD(i) = sqrt( abs( (x0 - xD(i))^2 + (y0 - yD(i))^2));
        if (distD(i) <  p_impactRange)
            % object destroyed
            D_OffBoard(i) = 1;
            D_happened(end+1) = s_step;
            all_happened(end+1) = s_step;
            D_happenedCurrentLife(end+1) = s_step;
        end
        if (distD(i) > 2 * p_sensorRange)
            % object restored
            D_OffBoard(i) = 0;
        end

        if (modulationPolicy == 1)
            if (distD(i) < 1 * p_sensorRange) && (D_OffBoard(i) == 0)
                % modulation = neutralMod;
            end
        else
            if (distD(i) < 1 * p_sensorRange) && (D_OffBoard(i) == 0)
                % modulation = p_posMod;
           end
        end
    end
end

% OBJECT E
if (p_nr_ins > 10)
    for i = 1:nr_E_objs
        distE_1(i) = distE(i);
        distE(i) = sqrt( abs( (x0 - xE(i))^2 + (y0 - yE(i))^2));
        if (distE(i) <  p_impactRange)
            % object destroyed
            E_OffBoard(i) = 1;
            E_happened(end+1) = s_step;
            all_happened(end+1) = s_step;
            E_happenedCurrentLife(end+1) = s_step;
        end
        if (distE(i) > 2 * p_sensorRange)
            % object restored
            E_OffBoard(i) = 0;
        end

        if (modulationPolicy == 1)
            if (distE(i) < 1 * p_sensorRange) && (E_OffBoard(i) == 0)
                % modulation = neutralMod;
            end
        else
            if (distE(i) < 1 * p_sensorRange) && (E_OffBoard(i) == 0)
                % modulation = p_posMod;
           end
        end
    end
end

% OBJECT F
if (p_nr_ins > 12)
    for i = 1:nr_F_objs
        distF_1(i) = distF(i);
        distF(i) = sqrt( abs( (x0 - xF(i))^2 + (y0 - yF(i))^2));
        if (distF(i) <  p_impactRange)
            % object destroyed
            F_OffBoard(i) = 1;
            F_happened(end+1) = s_step;
            all_happened(end+1) = s_step;
            F_happenedCurrentLife(end+1) = s_step;
        end
        if (distF(i) > 2 * p_sensorRange)
            % object restored
            F_OffBoard(i) = 0;
        end

        if (modulationPolicy == 1)
            if (distF(i) < 1 * p_sensorRange) && (F_OffBoard(i) == 0)
                % modulation = neutralMod;
            end
        else
            if (distF(i) < 1 * p_sensorRange) && (F_OffBoard(i) == 0)
                % modulation = p_posMod;
           end
        end
    end
end
% OBJECT G
if (p_nr_ins > 14)
    for i = 1:nr_G_objs
        distG_1(i) = distG(i);
        distG(i) = sqrt( abs( (x0 - xG(i))^2 + (y0 - yG(i))^2));
        if (distG(i) <  p_impactRange)
            % object destroyed
            G_OffBoard(i) = 1;
            G_happened(end+1) = s_step;
            all_happened(end+1) = s_step;
            G_happenedCurrentLife(end+1) = s_step;
        end
        if (distG(i) > 2 * p_sensorRange)
            % object restored
            G_OffBoard(i) = 0;
        end

        if (modulationPolicy == 1)
            if (distG(i) < 1 * p_sensorRange) && (G_OffBoard(i) == 0)
                % modulation = neutralMod;
            end
        else
            if (distG(i) < 1 * p_sensorRange) && (G_OffBoard(i) == 0)
                % modulation = p_posMod;
           end
        end
    end
end

% OBJECT H
if (p_nr_ins > 16)
    for i = 1:nr_H_objs
        distH_1(i) = distH(i);
        distH(i) = sqrt( abs( (x0 - xH(i))^2 + (y0 - yH(i))^2));
        if (distH(i) <  p_impactRange)
            % object destroyed
            H_OffBoard(i) = 1;
            H_happened(end+1) = s_step;
            all_happened(end+1) = s_step;
            H_happenedCurrentLife(end+1) = s_step;
        end
        if (distH(i) > 2 * p_sensorRange)
            % object restored
            H_OffBoard(i) = 0;
        end

        if (modulationPolicy == 1)
            if (distH(i) < 1 * p_sensorRange) && (H_OffBoard(i) == 0)
                % modulation = neutralMod;
            end
        else
            if (distH(i) < 1 * p_sensorRange) && (H_OffBoard(i) == 0)
                % modulation = p_posMod;
           end
        end
    end
end

% Set modulation to be 0.1 (positive) if modulation is 0
if modulation == 0;
    modulation = p_posMod;
end


% x0 and y0 represent location of agent
log_XYMod(:,s_step) = [x0 y0 modulation];
log_XYModDelivered(:,s_step) = [x0 y0 fifo_buffer(sizeOfBuffer)];
