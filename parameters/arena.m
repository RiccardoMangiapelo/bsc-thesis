
% Arena specific settings for agent simulation
arenaSettings;

% Calling user-defined parameters
userParameters;

serviceVariables;
% Draw static parts of the arena
if p_visual == 1
    drawArena;
end

tic;
while (s_step < p_STEPS)

    % During simulation, some changes (e.g the modulation policy) can be
    % enforced by adding conditions in this file
    onlineChanges;
    % Read all sensory information
    readSensorsInArena;
    % Compute output according to sensory information
    neuralComputation;
    % Move the agent in the environment according to the neural output, and
    % assign modulation values
    agentMoves;

    % Compute plasticity according to neural activity and modulation value
    plasticity;

    % Redraw dynamic parts of the arena
    if (p_visual == 1)
        if (mod(s_step,p_plotEveryNrSteps) == 0)
            drawArenaRefresh;
        end
    else
        if mod(s_step,round(p_STEPS/100)) == 0
            disp([num2str(round(s_step/p_STEPS * 100)) '% completed']);
        end
    end
    % Increment simulation step by 1
    s_step = s_step + 1;

end
