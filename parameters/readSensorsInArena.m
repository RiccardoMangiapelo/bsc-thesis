% Reading wall sensors ---------------------------------------------------%
    distLW = max([min([xL p_arenaWidth - xL yL 1 - yL]) 0]);
    distRW = max([min([xR p_arenaWidth - xR yR 1 - yR]) 0]);
    distW_1 = distW;
    distW = max([min([x0 p_arenaWidth - x0 y0 1 - y0]) 0]);
    wallSensor(1) = (p_sensorRange - min(distLW, p_sensorRange)) * 1/p_sensorRange;
    wallSensor(2) = (p_sensorRange - min(distRW, p_sensorRange)) * 1/p_sensorRange;


% Object distances -------------------------------------------------------%
    sensorA(1:2) = 0;
    sensorB(1:2) = 0;
    sensorC(1:2) = 0;
    sensorD(1:2) = 0;
    sensorE(1:2) = 0;
    sensorF(1:2) = 0;
    sensorG(1:2) = 0;
    sensorH(1:2) = 0;

% Distances from objects A
for i = 1:nr_A_objs
    distLA(i) = sqrt(abs( (xL - xA(i))^2 + (yL - yA(i))^2));
    distRA(i) = sqrt(abs( (xR - xA(i))^2 + (yR - yA(i))^2));
end

for i = 1:nr_A_objs
    if (A_OffBoard(i) == 0)
        if (distLA(i) < p_sensorRange)
            sensorA(1) = 1 - (distLA(i) / p_sensorRange);
        end
        if (distRA(i) < p_sensorRange)
            sensorA(2) = 1 - (distRA(i) / p_sensorRange);
        end
    end
end

% Distances from objects B
for i = 1:nr_B_objs
    distLtoB(i) = sqrt(abs( (xL - xB(i))^2 + (yL - yB(i))^2));
    distRtoB(i) = sqrt(abs( (xR - xB(i))^2 + (yR - yB(i))^2));
end
for i = 1:nr_B_objs
    if (B_OffBoard(i) == 0)
        if (distLtoB(i) < p_sensorRange)
            sensorB(1) = 1 - (distLtoB(i) / p_sensorRange);
        end
        if (distRtoB(i) < p_sensorRange)
            sensorB(2) = 1 - (distRtoB(i) / p_sensorRange);
        end
    end
end

% Distances from objects C
for i = 1:nr_C_objs
    distLtoC(i) = sqrt(abs( (xL - xC(i))^2 + (yL - yC(i))^2));
    distRtoC(i) = sqrt(abs( (xR - xC(i))^2 + (yR - yC(i))^2));
end

for i = 1:nr_C_objs
    if (C_OffBoard(i) == 0)
        if (distLtoC(i) < p_sensorRange)
            sensorC(1) = 1 - (distLtoC(i) / p_sensorRange);
        end
        if (distRtoC(i) < p_sensorRange)
            sensorC(2) = 1 - (distRtoC(i) / p_sensorRange);
        end
    end
end

% Distances from objects D
for i = 1:nr_D_objs
    distLtoD(i) = sqrt(abs( (xL - xD(i))^2 + (yL - yD(i))^2));
    distRtoD(i) = sqrt(abs( (xR - xD(i))^2 + (yR - yD(i))^2));
end

for i = 1:nr_D_objs
    if (D_OffBoard(i) == 0)
        if (distLtoD(i) < p_sensorRange)
            sensorD(1) = 1 - (distLtoD(i) / p_sensorRange);
        end
        if (distRtoD(i) < p_sensorRange)
            sensorD(2) = 1 - (distRtoD(i) / p_sensorRange);
        end
    end
end

% Distances from objects E
for i = 1:nr_E_objs
    distLtoE(i) = sqrt(abs( (xL - xE(i))^2 + (yL - yE(i))^2));
    distRtoE(i) = sqrt(abs( (xR - xE(i))^2 + (yR - yE(i))^2));
end

for i = 1:nr_E_objs
    if (E_OffBoard(i) == 0)
        if (distLtoE(i) < p_sensorRange)
            sensorE(1) = 1 - (distLtoE(i) / p_sensorRange);
        end
        if (distRtoE(i) < p_sensorRange)
            sensorE(2) = 1 - (distRtoE(i) / p_sensorRange);
        end
    end
end

% Distances from objects F
for i = 1:nr_F_objs
    distLtoF(i) = sqrt(abs( (xL - xF(i))^2 + (yL - yF(i))^2));
    distRtoF(i) = sqrt(abs( (xR - xF(i))^2 + (yR - yF(i))^2));
end

for i = 1:nr_F_objs
    if (F_OffBoard(i) == 0)
        if (distLtoF(i) < p_sensorRange)
            sensorF(1) = 1 - (distLtoF(i) / p_sensorRange);
        end
        if (distRtoF(i) < p_sensorRange)
            sensorF(2) = 1 - (distRtoF(i) / p_sensorRange);
        end
    end
end

% Distances from objects G
for i = 1:nr_G_objs
    distLtoG(i) = sqrt(abs( (xL - xG(i))^2 + (yL - yG(i))^2));
    distRtoG(i) = sqrt(abs( (xR - xG(i))^2 + (yR - yG(i))^2));
end

for i = 1:nr_G_objs
    if (G_OffBoard(i) == 0)
        if (distLtoG(i) < p_sensorRange)
            sensorG(1) = 1 - (distLtoG(i) / p_sensorRange);
        end
        if (distRtoG(i) < p_sensorRange)
            sensorG(2) = 1 - (distRtoG(i) / p_sensorRange);
        end
    end
end


% Distances from objects H
for i = 1:nr_H_objs
    distLtoH(i) = sqrt(abs( (xL - xH(i))^2 + (yL - yH(i))^2));
    distRtoH(i) = sqrt(abs( (xR - xH(i))^2 + (yR - yH(i))^2));
end

for i = 1:nr_H_objs
    if (H_OffBoard(i) == 0)
        if (distLtoH(i) < p_sensorRange)
            sensorH(1) = 1 - (distLtoH(i) / p_sensorRange);
        end
        if (distRtoH(i) < p_sensorRange)
            sensorH(2) = 1 - (distRtoH(i) / p_sensorRange);
        end
    end
end

% Lateral inhibition. This is also equivalent to assume that
% the sensors are 180 degree omnidirectional (not 360) -------------------%
    if wallSensor(1) > wallSensor(2)
      wallSensor(2) = 0;
    else
      wallSensor(1) = 0;
    end

    if sensorA(1) > sensorA(2)
       sensorA(2) = 0;
    else
      sensorA(1) = 0;
    end
    if sensorB(1) > sensorB(2)
        sensorB(2) = 0;
    else
       sensorB(1) = 0;
    end
    if sensorC(1) > sensorC(2)
        sensorC(2) = 0;
    else
        sensorC(1) = 0;
    end
    if sensorD(1) > sensorD(2)
        sensorD(2) = 0;
    else
        sensorD(1) = 0;
    end
    if sensorE(1) > sensorE(2)
        sensorE(2) = 0;
    else
        sensorE(1) = 0;
    end
    if sensorF(1) > sensorF(2)
        sensorF(2) = 0;
    else
        sensorF(1) = 0;
    end
    if sensorG(1) > sensorG(2)
        sensorG(2) = 0;
    else
        sensorG(1) = 0;
    end
    if sensorH(1) > sensorH(2)
        sensorH(2) = 0;
    else
        sensorH(1) = 0;
    end

% Building vector of all sensors -----------------------------------------%

    if (p_nr_ins > 2)
        sensor = [wallSensor sensorA'];
    end
    if (p_nr_ins > 4)
        sensor = [sensor sensorB'];
        sensorBconc6(s_step) = sensor(6);
    end
    if (p_nr_ins > 6)
        sensor = [sensor sensorC'];
    end
    if (p_nr_ins > 8)
        sensor = [sensor sensorD'];
    end
    if (p_nr_ins > 10)
        sensor = [sensor sensorE'];
    end
    if (p_nr_ins > 12)
        sensor = [sensor sensorF'];
    end
    if (p_nr_ins > 14)
        sensor = [sensor sensorG'];
    end
    if (p_nr_ins > 16)
        sensor = [sensor sensorH'];
    end

    % Adding sesor noise
    sensor = sensor +...
        (rand(stream, p_nr_ins, 1)' .* 2 .* p_transmissionNoise ) - p_transmissionNoise;

% Write  log
log_sensors(:,s_step) = sensor;
