for i=1:length(array_letters)
  nr_current_obj = 0;
  obj_around_current_obj = 0;
  switch array_letters(i)
    case 'C'
        nr_current_obj = nr_C_objs;
    case 'D'
        nr_current_obj = nr_D_objs;
    otherwise
        nr_current_obj = 0;
  end % of switch


  for c=1:nr_current_obj
    % place neutral objects randomly
      random_x = rand() * ( max_width - min_width_height ) + min_width_height; % random number in the range of max_width and min_width_height (plus the angel_devil_area)
      random_y = rand() * ( max_height - min_width_height ) + min_width_height;
      k = 1;
      while ( k<=length(existing_x) )
        % if ( ismember(existing_x(k),xA) | ismember(existing_x(k),xB) )
        %   far_away_value = angel_devil_area + distance_within_vip;
        % else
          far_away_value = distance_between_objects;
        % end % of ismember if

        if (abs( random_x - existing_x(k) ) <= far_away_value & ...
            abs( random_y - existing_y(k) ) <= far_away_value)

            random_x = rand() * ( max_width - min_width_height ) + min_width_height;
            random_y = rand() * ( max_height - min_width_height ) + min_width_height;
            k = 1;
        else
            k = k+1;
        end % of abs if

      end % of k loop

      % store newly calculated x and y to existing_x and existing_y
      existing_x(end+1)= random_x;
      existing_y(end+1)= random_y;
      % store newly calculated x and y to object array
      eval(['x' array_letters(i) '(c)' '=random_x;']);
      eval(['y' array_letters(i) '(c)' '=random_y;']);

  end % of c loop

end % of i loop
