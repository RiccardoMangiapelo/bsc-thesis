% Hebbian plasticity
HebbTerm = p_weightUpdateRate .* sensor' * output(:,s_step)';
% sensConc(:,s_step) = sensor';

% Traces
%                                                              + This is the hebbian_plasticity instead of RCHP because without threshold RCHP = hebbian_plasticity
shortTermHebb = (shortTermHebb) .* exp(-samplingTime/traceTC) + HebbTerm;


% Modulation of Hebbian
%                                        .* last element of fifo
modHebbTerm(:,s_step) = shortTermHebb  .* fifo_buffer(sizeOfBuffer);
