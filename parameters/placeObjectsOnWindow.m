
    for i = 1:nr_A_objs
        objA(i) = rectangle('Position',[xA(i)- p_objDrawRadius,yA(i)- p_objDrawRadius, 2* p_objDrawRadius, 2 * p_objDrawRadius],...
            'Curvature',[1,1], 'facecolor',[1 1 1], 'Linewidth',2);
    end

  %  for i = 1:nr_A_objs
  %      objAc(i) = rectangle('Position',[xA(i)- p_sensorRange,yA(i)- p_sensorRange, 2* p_sensorRange, 2 * p_sensorRange],...
  %          'Curvature',[1,1]);
  %  end

    for i = 1:nr_B_objs
        objB(i) = rectangle('Position',[xB(i)- p_objDrawRadius,yB(i)- p_objDrawRadius, 2* p_objDrawRadius, 2 * p_objDrawRadius],...
            'Curvature',[0,0], 'facecolor',[1 1 1], 'Linewidth',2);
    end

    for i = 1:nr_C_objs
        objC(i) = rectangle('Position',[xC(i)- p_objDrawRadius,yC(i)- p_objDrawRadius, 2* p_objDrawRadius, 2 * p_objDrawRadius],...
            'Curvature',[1,1], 'facecolor',[1 1 0.96], 'Linewidth',2);
    end

    for i = 1:nr_D_objs
        objD(i) = rectangle('Position',[xD(i)- p_objDrawRadius,yD(i)- p_objDrawRadius, 2* p_objDrawRadius, 2 * p_objDrawRadius],...
            'Curvature',[0,0], 'facecolor',[1 1 0.96], 'Linewidth',2);
    end

    for i = 1:nr_E_objs
        objE(i) = rectangle('Position',[xE(i)- p_objDrawRadius,yE(i)- p_objDrawRadius, 2* p_objDrawRadius, 2 * p_objDrawRadius],...
            'Curvature',[1,1], 'facecolor',[1 1 0.96], 'Linewidth',2, 'linestyle',':');
    end

    for i = 1:nr_F_objs
        objF(i) = rectangle('Position',[xF(i)- p_objDrawRadius,yF(i)- p_objDrawRadius, 2* p_objDrawRadius, 2 * p_objDrawRadius],...
            'Curvature',[0,0], 'facecolor',[1 1 0.96], 'Linewidth',2, 'linestyle',':');
    end

    for i = 1:nr_G_objs
        objG(i) = rectangle('Position',[xG(i)- p_objDrawRadius,yG(i)- p_objDrawRadius, 2* p_objDrawRadius, 2 * p_objDrawRadius],...
            'Curvature',[1,1], 'facecolor',[1 1 0.96], 'Linewidth',2, 'linestyle','-.');
    end

    for i = 1:nr_H_objs
        objH(i) = rectangle('Position',[xH(i)- p_objDrawRadius,yH(i)- p_objDrawRadius, 2* p_objDrawRadius, 2 * p_objDrawRadius],...
            'Curvature',[0,0], 'facecolor',[1 1 0.96], 'Linewidth',2, 'linestyle','-.');
    end
