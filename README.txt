Neural eligibility traces in navigation and food gathering scenarios: learning associations and predictors with delayed rewards


Riccardo Mangiapelo
Computer Science Undergraduate
Loughborough University, UK
r.mangiapelo-13@student.lboro.ac.uk

May 2017

-------------
1. LICENSE
-------------

This program is free software originally developed by Dr Andrea Soltoggio.
He gave permission on redistribute it and modify it under the terms of
the GNU General Public License version 3 as published
by the Free Software Foundation (LGPL may be granted upon request).

---------------------
2. USAGE and SUPPORT
---------------------

Expand the compressed file under an arbitrary user directory and set the Matlab current folder to it.

The experiment is launched by calling the script:

- DistalReward_Hebbian.m


This file launches the algorithm that produces the data shown in the report.
The user is required to press Enter to confirm the initial default
parameters. 
