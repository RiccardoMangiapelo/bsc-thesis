disp(['Braitenberg Vehicle Experiment']);
tic
clear;

addpath parameters;                                                        % Location of the parameters files

p_seedNumber = 1;                                                          % Seed for the pseudo-random number generation
stream = RandStream('mrg32k3a', 'Seed', p_seedNumber);
stream.reset;

init;                                                                      % Call the common-parameter initialisation


    clear p_firingValue;
    clear p_notFiringValue;

% Arena specific settings for agent simulation
arenaSettings;

% Calling user-defined parameters
userParameters;

% Initialising variables
serviceVariables;

main;

% this is repeated so that it calculates steps to saturation for the last life
lastResetStep(end+1) = s_step;
calculateSaturationStep;
howManyObj;
avg_A = mean(A_totObj);
avg_B = mean(B_totObj);
avg_C = mean(C_totObj);
avg_D = mean(D_totObj);
avg_E = mean(E_totObj);
avg_F = mean(F_totObj);
avg_G = mean(G_totObj);
avg_H = mean(H_totObj);

std_A = std(A_totObj);
std_B = std(B_totObj);
std_C = std(C_totObj);
std_D = std(D_totObj);
std_E = std(E_totObj);
std_F = std(F_totObj);
std_G = std(G_totObj);
std_H = std(H_totObj);

finalT = toc;


if p_visual == 0
    %save to file
% on my computer
    mkdir('/Users/riccardo/Desktop',num2str(p_cycleLength))
    filePath = strcat('/Users/riccardo/Desktop/',num2str(p_cycleLength));
    forwardSlash = '/';
% on GPU
    % mkdir('/home/corm/FYP_Experiment',num2str(p_cycleLength))
    % filePath = strcat('/home/corm/FYP_Experiment/',num2str(p_cycleLength));
    % forwardSlash = '/';
% on alioth
    % mkdir('C:\Users\localadmin\Documents\Riccardo_Mangiapelo_FYP\Simulations',num2str(p_cycleLength))
    % filePath = strcat('C:\Users\localadmin\Documents\Riccardo_Mangiapelo_FYP\Simulations\',num2str(p_cycleLength));
    % forwardSlash = '\';
% on dale-research
    % mkdir('/home/dale-research/Documents/Riccardo_Mangiapelo_FYP/Simulations',num2str(p_cycleLength))
    % filePath = strcat('/home/dale-research/Documents/Riccardo_Mangiapelo_FYP/Simulations/',num2str(p_cycleLength));
    % forwardSlash = '/';

    saveTXT = strcat(filePath,forwardSlash,'Specs.txt');

    specLife = strcat('Lives: ',num2str(p_totLives));
    specSteps = strcat('Steps per lifetime: ',num2str(p_cycleLength));
    specPolicy = strcat('Policy switch? (no/yes)(0/1): ',num2str(p_policySwitch));
    specInitialPolicy = strcat('Initial policy (1/2): ',num2str(modulationPolicy));
    elapsedTime = strcat('Elapsed time is ',num2str(finalT),' seconds.');

    specs = strcat(specLife,'\n',specSteps,'\n',specPolicy,'\n',specInitialPolicy,'\n',elapsedTime);
    fid = fopen(saveTXT,'wt');
    fprintf(fid, specs);
    fclose(fid);

    nameOfVars = strcat(filePath,forwardSlash,'objs_happened_STATS');
    save(nameOfVars,'A_happened','B_happened','C_happened','D_happened','E_happened',...
                    'F_happened','G_happened','H_happened', 'all_happened', ...
                    'A_totObj','B_totObj', 'C_totObj', 'D_totObj', 'E_totObj', 'F_totObj', ...
                    'G_totObj', 'H_totObj','all_tot', ...
                    'avg_A','avg_B','avg_C','avg_D','avg_E','avg_F','avg_G','avg_H',...
                    'std_A','std_B','std_C','std_D','std_E','std_F','std_G','std_H');

    plotWeights;
    if p_totLives == 1 && p_policySwitch == 0
        plotNavigationProbDensity;
    else
        plotModulation;
    end
    %plotTraces;
    % plotSteerIntensity;
    % plotWeightSaturation;
    plotHistogramOfSaturation;
    plotAgentPath;
    plotHistNumOfStep;
    % plotObjTraceWeight;
end

% disp(['Average number of steps between objects: ' num2str(stebtwObjs)]);

endSimulation;
